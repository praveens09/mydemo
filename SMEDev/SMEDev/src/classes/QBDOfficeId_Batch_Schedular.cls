global class QBDOfficeId_Batch_Schedular implements Schedulable {

   global void execute(SchedulableContext SC) {
      QBDOfficeId_PR_Daily_Update_Batch aa = new QBDOfficeId_PR_Daily_Update_Batch(false);
      Database.executeBatch(aa);
   }
}