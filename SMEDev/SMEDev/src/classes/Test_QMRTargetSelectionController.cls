@isTest
public class Test_QMRTargetSelectionController{
    
    public static testMethod void myTest(){
        List<Account> accList = new List<Account>();
        List<Opportunity> oppList = new List<Opportunity>();
        List<Proposal__c> propList = new List<Proposal__c>();
        List<Contract__c> contrList = new List<Contract__c>();
        // Custom setting creation
        TestUtilityDataClassQantas.enableTriggers();
        
        //Accounts
        accList = TestUtilityDataClassQantas.getAccounts();
        
        // Load Opportunities
         oppList = TestUtilityDataClassQantas.getOpportunities(accList);
         
        // Load Proposals
        propList = TestUtilityDataClassQantas.getProposal(oppList);
        
        // Create Fare Structure
        Fare_Structure__c fs = new Fare_Structure__c(Name = 'J', Active__c = true, Cabin__c = 'Business', 
                                                     Category__c = 'Mainline', Market__c = 'Australia', 
                                                     ProductCode__c = 'DOM-0001', Qantas_Published_Airfare__c = 'JBUS',
                                                     Segment__c = 'Domestic'
                                                     );
        try{
            //Database.Insert(fs);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        
        contrList = TestUtilityDataClassQantas.getContracts(propList);
        
        // QMR Target Data
        List<QMR_Target__c> domList = new List<QMR_Target__c>();
        List<QMR_Target__c> intList = new List<QMR_Target__c>();
        
        QMR_Target__c qmrT1 = new QMR_Target__c();
        qmrT1.Contract__c = contrList[0].Id;
        qmrT1.Minimum_Amount__c = 1;
        qmrT1.Maximum_Amount__c = 2;
        qmrT1.Region__c = 'Domestic';
        
        QMR_Target__c qmrT2 = new QMR_Target__c();
        qmrT2.Contract__c = contrList[0].Id;
        qmrT2.Minimum_Amount__c = 1;
        qmrT2.Maximum_Amount__c = 2;
        qmrT2.Region__c = 'Domestic';
        
        QMR_Target__c qmrT3 = new QMR_Target__c();
        qmrT3.Contract__c = contrList[0].Id;
        qmrT3.Minimum_Amount__c = 1;
        qmrT3.Maximum_Amount__c = 2;
        qmrT3.Region__c = 'Domestic';
        
        domList.add(qmrT3);
        domList.add(qmrT2);
        domList.add(qmrT1);
        
        QMR_Target__c qmrTI1 = new QMR_Target__c();
        qmrTI1.Contract__c = contrList[0].Id;
        qmrTI1.Minimum_Amount__c = 1;
        qmrTI1.Maximum_Amount__c = 2;
        qmrTI1.Region__c = 'International';
        
        QMR_Target__c qmrTI2 = new QMR_Target__c();
        qmrTI2.Contract__c = contrList[0].Id;
        qmrTI2.Minimum_Amount__c = 1;
        qmrTI2.Maximum_Amount__c = 2;
        qmrTI2.Region__c = 'International';
        
        QMR_Target__c qmrTI3 = new QMR_Target__c();
        qmrTI3.Contract__c = contrList[0].Id;
        qmrTI3.Minimum_Amount__c = 1;
        qmrTI3.Maximum_Amount__c = 2;
        qmrTI3.Region__c = 'International';
        
        intList.add(qmrTI3);
        intList.add(qmrTI2);
        intList.add(qmrTI1);        
        
        try{
            Database.Insert(intList);
            Database.Insert(domList);
        }Catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        
        Test.startTest();
        ApexPages.StandardController stdController = new ApexPages.StandardController(contrList[0]);
        
        QMRTargetSelectionController qmrTarget = new QMRTargetSelectionController(stdController);
        qmrTarget.domTargetList = domList;
        qmrTarget.intTargetList = intList;
        qmrTarget.getExisitngvalues(contrList[0].Id);
        qmrTarget.addDomRows();
        qmrTarget.addIntRows();
        System.debug('***** DOM & INT '+qmrTarget.domTargetList+'&&&&'+qmrTarget.intTargetList);        
        qmrTarget.saveTargets();
        qmrTarget.removeDomRows();
        qmrTarget.removeIntRows();
        qmrTarget.callCancel();
        
        Test.stopTest();        
        
    }
    
}