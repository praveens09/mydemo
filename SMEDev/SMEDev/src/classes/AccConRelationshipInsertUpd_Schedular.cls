global class AccConRelationshipInsertUpd_Schedular implements Schedulable {

   public static String sched = '0 30 03 * * ?'; 
   
   global static String scheduleMe() {
        AccConRelationshipInsertUpd_Schedular SC = new AccConRelationshipInsertUpd_Schedular (); 
        return System.schedule('My batches', sched, SC);
    }
    
   global void execute(SchedulableContext SC) {
      AccConRelationshipInsertUpd_Batch aa = new AccConRelationshipInsertUpd_Batch();
      Database.executeBatch(aa,1);
   }
}