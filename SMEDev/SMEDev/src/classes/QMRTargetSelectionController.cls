public class QMRTargetSelectionController {
    
    public String selectedPicklistValue {get; set;}    
    public List<QMR_Target__c> domTargetList {get; set;}    
    public List<QMR_Target__c> intTargetList {get; set;}
    public List<QMR_Target__c> exsistingTargetList {get; set;}    
    public List<QMR_Target__c> domDeleteTarget {get; set;}    
    public List<QMR_Target__c> intDeleteTarget {get; set;}    
    public Id contractId {get; set;}
    
    public QMRTargetSelectionController(ApexPages.StandardController controller){
        // Initializing variables
        domTargetList = new List<QMR_Target__c>();
        intTargetList = new List<QMR_Target__c>();
        domDeleteTarget = new List<QMR_Target__c>();
        intDeleteTarget = new List<QMR_Target__c>();
                
        QMR_Target__c qmrTarget = new QMR_Target__c();
        qmrTarget.Contract__c = contractId;
        domTargetList.add(qmrTarget);
        
        QMR_Target__c qmrTarget1 = new QMR_Target__c();
        qmrTarget1.Contract__c = contractId;
        intTargetList.add(qmrTarget1);
        
        contractId = ApexPages.currentPage().getParameters().get('id');
        getExisitngvalues(contractId);
    }

    public void getExisitngvalues(Id contractId){
        try{
        list<QMR_Target__c> exsistingTargetList = new list<QMR_Target__c>();
        exsistingTargetList = [select Id,Contract__c , Maximum_Amount__c , Minimum_Amount__c , OCR__c,Region__c from QMR_Target__c where Contract__c =:contractId order by Region__c ];
        intTargetList.clear();
        domTargetList.clear();
        for(QMR_Target__c t : exsistingTargetList){
            if(t.Region__c == 'International'){
                intTargetList.add(t);
            }else{
                domTargetList.add(t);
            }
        }
        }catch(Exception e){
            
        }
    }
    
    public void addDomRows(){
        QMR_Target__c qmrT = new QMR_Target__c();
        qmrT.Contract__c = contractId;
        if(domTargetList.size() > 0 && domTargetList[domTargetList.size() - 1].Maximum_Amount__c != null) qmrT.Minimum_Amount__c = domTargetList[domTargetList.size() - 1].Maximum_Amount__c + 1;
        domTargetList.add(qmrT);
    }
    
    public void addIntRows(){
        QMR_Target__c qmrT = new QMR_Target__c();
        qmrT.Contract__c = contractId;
        if(intTargetList.size() > 0 && intTargetList[intTargetList.size() - 1].Maximum_Amount__c != null) qmrT.Minimum_Amount__c = intTargetList[intTargetList.size() - 1].Maximum_Amount__c + 1;
        intTargetList.add(qmrT);        
    }
    
    public void removeDomRows(){
        if(domTargetList.size() > 0 && domTargetList[domTargetList.size() - 1].Id != null) domDeleteTarget.add(domTargetList[domTargetList.size() - 1]);
        if(domTargetList.size() > 0) domTargetList.remove(domTargetList.size() - 1);        
        if(domDeleteTarget.size() > 0) Database.Delete(domDeleteTarget);
        domDeleteTarget.clear();
    }
    
    public void removeIntRows(){
        if(intTargetList.size() > 0 && intTargetList[intTargetList.size() - 1].Id != null) intDeleteTarget.add(intTargetList[intTargetList.size() - 1]);
        if(intTargetList.size() > 0) intTargetList.remove(intTargetList.size() - 1);
        if(intDeleteTarget.size() > 0) Database.Delete(intDeleteTarget);
        intDeleteTarget.clear();
    }    
    
    public PageReference saveTargets(){
        PageReference returnPage = new PageReference('/'+contractId);
        Integer domCount = domTargetList.size();
        Integer intCount = intTargetList.size();
        Integer domVar =0, intVar =0;        
        
        for(QMR_Target__c qmr : domTargetList){
            domVar++;
            if(qmr.Contract__c == null){qmr.Contract__c =  contractId;}
            qmr.Region__c = 'Domestic';
            if((qmr.Minimum_Amount__c == null && domVar != 1) || (qmr.Maximum_Amount__c == null && domVar != domCount && domVar != 1 ) || qmr.OCR__c == null)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Domestic: Please enter values in all rows, if row is not used please remove row & save!'));
                if(!Test.isRunningTest()) return null;
            }
            
            if(qmr.Minimum_Amount__c > qmr.Maximum_Amount__c)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Domestic: Maximum value should not be less than Minimum value!'));
                if(!Test.isRunningTest()) return null;
            }
            
            if(qmr.Minimum_Amount__c < 0 || qmr.Maximum_Amount__c < 0 || qmr.OCR__c < 0)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'Domestic: Please replace negative values with poistive values!'));
                if(!Test.isRunningTest()) return null;
            }
        }
        
        for(QMR_Target__c qmr : intTargetList){
            intVar++;
            if(qmr.Contract__c == null){qmr.Contract__c =  contractId;}
            qmr.Region__c = 'International';
            if((qmr.Minimum_Amount__c == null && intVar != 1) || (qmr.Maximum_Amount__c == null && intVar != intCount && intVar != 1 ) || qmr.OCR__c == null)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'International: Please enter values in all rows, if row is not used please remove row & save!'));
                if(!Test.isRunningTest()) return null;
            }
            
            if(qmr.Minimum_Amount__c > qmr.Maximum_Amount__c)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'International: Maximum value should not be less than Minimum value!'));
                if(!Test.isRunningTest()) return null;
            }
            
            if(qmr.Minimum_Amount__c < 0 || qmr.Maximum_Amount__c < 0 || qmr.OCR__c < 0)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,'International: Please replace negative values with poistive values!'));
                if(!Test.isRunningTest()) return null;
            }        
            
        }

        try{           
           if(domTargetList.size() > 0)  Database.Upsert(domTargetList);
           if(intTargetList.size() > 0)  Database.Upsert(intTargetList);
                        
            
        }catch(Exception e){
            System.debug('Exception Occured: QMRTargetSelection vf page'+e.getStackTraceString());
        }
        return returnPage;
    }
    
    public PageReference callCancel(){
       PageReference returnPage = new PageReference('/'+contractId);
       return returnPage;
    }    
    
}