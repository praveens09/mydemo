/*
 * This class contains unit tests for validating the behavior of Account_Ownership_Update_Daily_Batch class.
 */
@isTest
public class Test_AccConRelationshipInsertUpd_Batch {

    static testMethod void testBatchProcess() {
    Database.BatchableContext BC;
    Set<ID> ids = new Set<ID>();
    List<Staging_Account_Contact_Relationship__c> allACR = new List<Staging_Account_Contact_Relationship__c>();
    List<AccountContactRelation> AccConRel = new List<AccountContactRelation>();
           // TestUtilityDataClassQantas.enableTriggers();
           for(Integer i=0;i<50;i++)
           allACR.add(new Staging_Account_Contact_Relationship__c(AQ_Staging_Flag__c = 'I',Account__c ='0019000001Q7W9k', Contact__c='0039000001lk3NR'));
           insert allACR;
               for (Staging_Account_Contact_Relationship__c stgc : allACR )
                ids.add(stgc.id);
        AccountContactRelation newAcc = new AccountContactRelation( AQ_Account_Nominee_Air_Traveller_Status__c = True,
                                                                    AccountId = '0019000001Q7W9k',
                                                                    ContactId = '0039000001lk3NR',
                                                                    AQ_Account_Holder_Flag__c = True,
                                                                    AQ_Account_Nominee_customer_Id__c = '2112',
                                                                    AQ_Account_Nominee_Status_reason__c = '',
                                                                    AQ_Account_Nominee_User_Flag__c = True,
                                                                    AQ_Contact_Role__c = '',
                                                                    AQ_Key_Decision_Maker__c = True,
                                                                    AQ_Key_Travel_Coordinator__c = True,
                                                                    AQ_Membership_Number__c = '3444',
                                                                    AQ_Promotions__c = True,
                                                                    AQ_Status__c = 'Active',
                                                                    AQ_Updates_Program_News__c = True,
                                                                    AQ_Frequent_Flyer_Number__c = '4428326',
                                                                    AQ_Phone__c = '',
                                                                    AQ_Points_Balance_Account_Information__c = False
                                                                    );  
           
        insert newAcc ; 
  
       /* AccountContactRelation AccConRelToUpdates = new AccountContactRelation( AccountId = '001N000000naxSX',
                                                                    ContactId = '003N000000ikZND',
                                                                   // Staging_ID__c = '07kN00000004oQq',                                                      
                                                                    AQ_Account_Nominee_Air_Traveller_Status__c = False,
                                                                    AQ_Account_Holder_Flag__c = True,
                                                                    AQ_Account_Nominee_customer_Id__c = '0002112',
                                                                    AQ_Account_Nominee_Status_reason__c = '',
                                                                    AQ_Account_Nominee_User_Flag__c = True,
                                                                    AQ_Contact_Role__c = '',
                                                                    AQ_Key_Decision_Maker__c = True,
                                                                    AQ_Key_Travel_Coordinator__c = True,
                                                                    AQ_Membership_Number__c = '1233444',
                                                                    AQ_Promotions__c = True,
                                                                    AQ_Status__c = 'Active',
                                                                    AQ_Updates_Program_News__c = True,
                                                                    AQ_Frequent_Flyer_Number__c = '4428326000',
                                                                    AQ_Phone__c = '',
                                                                    AQ_Points_Balance_Account_Information__c = False ); */
                                                                                                                  
       newAcc.AQ_Account_Nominee_Air_Traveller_Status__c = False;
       newAcc.AQ_Account_Holder_Flag__c = True;
       newAcc.AQ_Account_Nominee_customer_Id__c = '0002112';                                                            
        
     //   Update AccConRelToUpdates; 
        Update  newAcc;
        
        Test.startTest();
            AccConRelationshipInsertUpd_Batch batchObj = new AccConRelationshipInsertUpd_Batch();
            Database.executeBatch(batchObj);
        Test.stopTest();
       }
}