global class QBDOfficeId_PR_Daily_Update_Batch implements Database.Batchable<sObject> {
    
    Boolean cSettingChange;
    String query = 'SELECT Id, Name, QBD__c, Aquire_Blocking_Classification__c, Airline_Level__c, Aquire_System__c, Aquire_Eligibility_f__c, (SELECT Id, Key_Contact_Office_ID__c, Active__c FROM Product_Registrations__r where RecordType.Name=\'QBD Registration\') FROM Account where Active__c=true ';
    
    global QBDOfficeId_PR_Daily_Update_Batch(Boolean cSetting){
        cSettingChange = cSetting;
        if(!cSettingChange) query = query +'AND Process_QBD_Update__c = true';
        if(cSettingChange) query = query +'AND QBD__c = true';    
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('Query is : '+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext ctx, List<Account> filteredAccnts){
    
        List<Product_Registration__c> updatePR = new List<Product_Registration__c>();        
        //QBD_Online_Office_ID__c officeID = QBD_Online_Office_ID__c.getValues('QBD');
        
        // For loop to iterate all the Accounts
        for(Account acc : filteredAccnts){
            
            for(Product_Registration__c productReg : acc.Product_Registrations__r){
            Boolean addToList = false;
           /* if(acc.QBD__c == true && acc.Aquire_Blocking_Classification__c == true && acc.Aquire_System__c == true && acc.Aquire_Eligibility_f__c == 'Y' && productReg.Active__c == true){
                // Scenario 1 as per QBS OfficeIds Rules CRM - 352
                productReg.Key_Contact_Office_ID__c = '';
            }else if(acc.QBD__c == true && acc.Aquire_Blocking_Classification__c == true && acc.Aquire_System__c == true && acc.Aquire_Eligibility_f__c == 'N' && productReg.Active__c == true){
                // Scenario 2 as per QBS OfficeIds Rules CRM - 352
                productReg.Key_Contact_Office_ID__c = '';
            }else if(acc.QBD__c == true && acc.Aquire_Blocking_Classification__c == true && acc.Aquire_System__c == false && acc.Aquire_Eligibility_f__c == 'N' && productReg.Active__c == true){
                // Scenario 3 as per QBS OfficeIds Rules CRM - 352
                productReg.Key_Contact_Office_ID__c = '';
            }else 
            if(acc.QBD__c == true && acc.Aquire_Blocking_Classification__c == false && acc.Aquire_System__c == true && acc.Aquire_Eligibility_f__c == 'Y' && productReg.Active__c == true && productReg.Key_Contact_Office_ID__c != officeID.Key_Contact_Office_ID__c){                
                //Scenario 5 as per QBS OfficeIds Rules CRM - 352
                productReg.Key_Contact_Office_ID__c = officeID.Key_Contact_Office_ID__c;
                addToList = true;
            }else if(acc.QBD__c == true && acc.Aquire_Blocking_Classification__c == false && acc.Aquire_System__c == true && acc.Aquire_Eligibility_f__c == 'N' && productReg.Active__c == true && productReg.Key_Contact_Office_ID__c != '' && productReg.Key_Contact_Office_ID__c != null){
                //Scenario 6 as per QBS OfficeIds Rules CRM - 352
                productReg.Key_Contact_Office_ID__c = '';
                addToList = true;
            }else if(acc.QBD__c == true && acc.Aquire_Blocking_Classification__c == false && acc.Aquire_System__c == false && acc.Aquire_Eligibility_f__c == 'N' && productReg.Active__c == true && productReg.Key_Contact_Office_ID__c != '' && productReg.Key_Contact_Office_ID__c != null){
                //Scenario 7 as per QBS OfficeIds Rules CRM - 352
                productReg.Key_Contact_Office_ID__c = '';
                addToList = true;
            }else if(acc.QBD__c == true && acc.Aquire_Blocking_Classification__c == false && acc.Aquire_System__c == false && acc.Aquire_Eligibility_f__c == 'Y' && productReg.Active__c == true && productReg.Key_Contact_Office_ID__c != '' && productReg.Key_Contact_Office_ID__c != null){
                //Scenario 8 as per QBS OfficeIds Rules CRM - 352
                productReg.Key_Contact_Office_ID__c = '';
                addToList = true;
            }else if(productReg.Active__c == false && productReg.Key_Contact_Office_ID__c != '' && productReg.Key_Contact_Office_ID__c != null){
                // If QBD Product Registration is Changed to In active, remove the Contact Office ID
                productReg.Key_Contact_Office_ID__c = '';
                addToList = true;
            }  */

            if(acc.Airline_Level__c != Null){
                QBD_Online_Office_ID__c officeID = QBD_Online_Office_ID__c.getValues(acc.Airline_Level__c);
                if(officeID != Null){
                    productReg.Key_Contact_Office_ID__c = officeID.Key_Contact_Office_ID__c;
                }
            }
            
            // Adding individual Product Registration record to list to update.
            if(addToList) updatePR.add(productReg);
            }            
            
            // Flag off Process QBD Update on Accounts
            if(!cSettingChange) acc.Process_QBD_Update__c = false;   
        }
        
        try{
            // Update Product Registration List
            if(updatePR.size() > 0) Database.Update(updatePR);
            // Update Accounts
            if(filteredAccnts.size() > 0 && !cSettingChange) Database.Update(filteredAccnts);
        }catch(Exception e){
            System.debug('Exception Occured: '+e.getMessage());
        }
        
    }
    
    global void finish(Database.BatchableContext ctx){
         if(!cSettingChange){
            List<QBD_Online_Office_ID__c> customSettingChange = [SELECT Id, Key_Contact_Office_ID__c FROM QBD_Online_Office_ID__c where LastModifiedDate = TODAY Limit 1];
            if(customSettingChange.size() > 0){
                QBDOfficeId_PR_Daily_Update_Batch aa = new QBDOfficeId_PR_Daily_Update_Batch(true);
                Database.executeBatch(aa);            
            }
         }
    }   

}