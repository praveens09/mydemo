public class CreateLogs {
   
    public static void createApplicationLog(String recId, Exception mainExc, List<String> lstErrMessages, Map<string, String> mapMessage){
       try{
       		Application_Error_Log__c objErrLog = new Application_Error_Log__c();
       		objErrLog.Exception_Type__c = mainExc.getTypeName();
       		objErrLog.Error_Message__c = mainExc.getMessage();
            objErrLog.Error_Message__c += ' ' + mainExc.getStackTraceString();
            objErrLog.Business_Function_Name__c = lstErrMessages[0];
            objErrLog.Class_Name__c = lstErrMessages[1];
            objErrLog.Method_Name__c = lstErrMessages[2];
            objErrLog.LoggedIn_User__c = lstErrMessages[3];
            objErrLog.Failed_Record_ID__c = recId;
            
            if(mapMessage != Null && mapMessage.containsKey('Inbound')){
            	objErrLog.Inbound_Payload__c = mapMessage.get('Inbound');
            }

            if(mapMessage != Null && mapMessage.containsKey('Outbound')){
            	objErrLog.Outbound_Payload__c = mapMessage.get('Outbound');
            }
            
       		insert objErrLog;
        }
        catch(Exception ex){
       		system.debug('Error while creating Error Logs'+ex);
        }
    }
    
    /*--------------------------------------------------------------------------------------      
    Method Name:        createIntegrationLog
    Description:        Method to Create Integration Logs
    --------------------------------------------------------------------------------------*/    
    public static Integration_Logs__c  createIntegrationLog(String type, String msg, String  serviceType, 
                                                            String objectId, Boolean isException){

        Integration_Logs__c  objIntLog = new Integration_Logs__c();
        objIntLog.Type__c = type;
        objIntLog.Request_Response__c = msg;
        objIntLog.Service_Type__c = serviceType;
        objIntLog.Is_Exception__c = isException;
        objIntLog.SObject_ID__c = objectId;
        objIntLog.Sent_Received_Date_Time__c = system.now();
        return objIntLog;
    } 
}