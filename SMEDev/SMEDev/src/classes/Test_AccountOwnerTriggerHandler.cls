@isTest
public class Test_AccountOwnerTriggerHandler{

    public static testMethod void myTest(){
        List<User> twoUsers = new List<User>();
        
        // Custom setting creation
        TestUtilityDataClassQantas.enableTriggers();
        
        // Users
        twoUsers = TestUtilityDataClassQantas.getUsers('Qantas Sales', 'MAS Team', '');        
        
        // Account Owner object data creation    
        Account_Owner__c ao = new Account_Owner__c(Name = 'S', Account_Manager__c = UserInfo.getUserId(), Business_Development_Rep__c = twoUsers[0].Id);
        
        try{
            Database.Insert(ao);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        
        ao.Account_Manager__c = twoUsers[0].Id;
        ao.Business_Development_Rep__c = UserInfo.getUserId();
        
        try{
            Database.Update(ao);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }
        
    }
}