/**********************************************************************************************************************************
 
    Created Date: 15/08/16 (DD/MM/YYYY)
    //Added By : Priyadharshini Vellingiri
    Description: Batch job for Aquire. Data will be fetched from the staging Account contact relationship and that will be Updated/Inserted in the Account contact relationship Object
  
    Versión:
    V1.0 - 15/08/16 - Initial version
 
**********************************************************************************************************************************/
global class AccConRelationshipInsertUpd_Batch implements database.Batchable<sObject> {
        global String [] email = new String[] {'priyadharshinivellingiri@qantas.com.au'};
         String query='select id,Name,AQ_Airline_News_and_Promotions__c,AQ_ABN__c,AQ_Salutation__c,AQ_First_Name__c,AQ_LastName__c,AQ_Email__c,AQ_Phone__c,AQ_Job_Title__c,Related_Email__c,Related_Phone__c,Account__c,Contact__c,AQ_Account_Nominee_customer_Id__c,AQ_Account_Nominee_Air_Traveller_Status__c,AQ_Account_Holder_Flag__c,AQ_Account_Nominee_Status_reason__c,AQ_Account_Nominee_User_Flag__c,AQ_Contact_Role__c,AQ_Key_Decision_Maker__c,AQ_Key_Travel_Coordinator__c,AQ_Membership_Number__c,AQ_Promotions__c,AQ_Status__c,AQ_Updates_Program_News__c,AQ_Frequent_Flyer_Number__c,AQ_Points_Balance_Account_Information__c,AQ_Staging_Flag__c,Staging_ID__c FROM Staging_Account_Contact_Relationship__c WHERE lastmodifieddate =Today';
          List<AccountContactRelation> AccConRelToUpdates1 = new List<AccountContactRelation>();
         List<AccountContactRelation> AccConRelToInsert = new List<AccountContactRelation>();
        
     /*   global AccConRelationshipInsertUpd_Batch(){
            allACR = [SELECT id,Name,AQ_Airline_News_and_Promotions__c,Account__c,Contact__c,AQ_Account_Nominee_customer_Id__c,AQ_Account_Nominee_Air_Traveller_Status__c,AQ_Account_Holder_Flag__c,AQ_Account_Nominee_Status_reason__c,AQ_Account_Nominee_User_Flag__c,AQ_Contact_Role__c,AQ_Key_Decision_Maker__c,AQ_Key_Travel_Coordinator__c,AQ_Membership_Number__c,AQ_Promotions__c,AQ_Status__c,AQ_Updates_Program_News__c,AQ_Frequent_Flyer_Number__c,AQ_Points_Balance_Account_Information__c,AQ_Phone__c,AQ_Staging_Flag__c,Staging_ID__c FROM Staging_Account_Contact_Relationship__c
                      WHERE LastModifiedDate = Today];                         
        } */
        
        global Database.Querylocator start(Database.BatchableContext bc){
            //String query='select id,Name from Staging_Account_Contact_Relationship__c';
            return database.getQueryLocator(query);
        }
        global void execute(Database.BatchableContext bc , List<Staging_Account_Contact_Relationship__c> scope) {                           
            for(Staging_Account_Contact_Relationship__c a :scope)
           {            
           AccountContactRelation newAcc = new AccountContactRelation();      
           AccountContactRelation AccConRelToUpdates = new AccountContactRelation();         
              if(a.AQ_Staging_Flag__c =='I') {
              System.debug('***Staging Flag***'+a.AQ_Staging_Flag__c);
              
              newAcc.AQ_Airline_News_and_Promotions__c = a.AQ_Airline_News_and_Promotions__c;
              newAcc.AQ_ABN__c = a.AQ_ABN__c;
              newAcc.AQ_Salutation__c = a.AQ_Salutation__c;
              newAcc.AQ_First_Name__c = a.AQ_First_Name__c;
              newAcc.AQ_Last_Name__c = a.AQ_LastName__c;
              newAcc.AQ_Email__c = a.AQ_Email__c;
              newAcc.AQ_Phone__c = a.AQ_Phone__c;
              newAcc.AQ_Job_Title__c = a.AQ_Job_Title__c;
              newAcc.Related_Email__c = a.Related_Email__c;
              newAcc.Related_Phone__c = a.Related_Phone__c;
              newAcc.AccountId = a.Account__c;
              newAcc.ContactId = a.Contact__c;
              newAcc.AQ_Account_Nominee_Air_Traveller_Status__c = a.AQ_Account_Nominee_Air_Traveller_Status__c;
              newAcc.AQ_Account_Holder_Flag__c = a.AQ_Account_Holder_Flag__c; 
              newAcc.AQ_Account_Nominee_customer_Id__c = a.AQ_Account_Nominee_customer_Id__c;
              newAcc.AQ_Account_Nominee_Status_reason__c = a.AQ_Account_Nominee_Status_reason__c;
              newAcc.AQ_Account_Nominee_User_Flag__c = a.AQ_Account_Nominee_User_Flag__c;
              newAcc.AQ_Contact_Role__c = a.AQ_Contact_Role__c; 
              newAcc.AQ_Key_Decision_Maker__c = a.AQ_Key_Decision_Maker__c;
              newAcc.AQ_Key_Travel_Coordinator__c = a.AQ_Key_Travel_Coordinator__c;
              newAcc.AQ_Membership_Number__c = a.AQ_Membership_Number__c;
              newAcc.AQ_Promotions__c = a.AQ_Promotions__c; 
              newAcc.AQ_Status__c = a.AQ_Status__c;
              newAcc.AQ_Updates_Program_News__c = a.AQ_Updates_Program_News__c;
              newAcc.AQ_Frequent_Flyer_Number__c = a.AQ_Frequent_Flyer_Number__c;
              newAcc.AQ_Points_Balance_Account_Information__c = a.AQ_Points_Balance_Account_Information__c;
              AccConRelToInsert.add(newAcc);
              System.debug('***Insertrecords***'+AccConRelToInsert);
             }  
   
       if(a.AQ_Staging_Flag__c =='U' && a.Staging_ID__c <> '') {
                  System.debug('***Staging Flag***'+a.AQ_Staging_Flag__c);
                  System.debug('***Staging ID***'+a.Staging_ID__c);
                  
                  //AccountContactRelation acr = [select id,AQ_Airline_News_and_Promotions__c,AQ_Account_Nominee_Air_Traveller_Status__c,AQ_Account_Holder_Flag__c,AQ_Account_Nominee_customer_Id__c,AQ_Account_Nominee_Status_reason__c,AQ_Account_Nominee_User_Flag__c,AQ_Contact_Role__c,AQ_Key_Decision_Maker__c,AQ_Key_Travel_Coordinator__c,AQ_Membership_Number__c,AQ_Promotions__c,AQ_Status__c,AQ_Updates_Program_News__c,AQ_Phone__c,AQ_Frequent_Flyer_Number__c,AQ_Points_Balance_Account_Information__c from AccountContactRelation where id =:a.Staging_ID__c];
                  //AccConRelToUpdates.id = a.Staging_ID__c;
                  
                  AccConRelToUpdates.AQ_Airline_News_and_Promotions__c  = a.AQ_Airline_News_and_Promotions__c;
                  AccConRelToUpdates.AQ_ABN__c = AccConRelToUpdates.AQ_ABN__c;
                  AccConRelToUpdates.AQ_Salutation__c = a.AQ_Salutation__c;
                  AccConRelToUpdates.AQ_First_Name__c = a.AQ_First_Name__c;
                  AccConRelToUpdates.AQ_Last_Name__c = a.AQ_LastName__c;
                  AccConRelToUpdates.AQ_Email__c = a.AQ_Email__c;
                  AccConRelToUpdates.AQ_Phone__c = a.AQ_Phone__c;
                  AccConRelToUpdates.AQ_Job_Title__c = a.AQ_Job_Title__c;
                  AccConRelToUpdates.Related_Email__c = a.Related_Email__c;
                  AccConRelToUpdates.Related_Phone__c = a.Related_Phone__c;
                  AccConRelToUpdates.AQ_Account_Nominee_Air_Traveller_Status__c = a.AQ_Account_Nominee_Air_Traveller_Status__c;
                  AccConRelToUpdates.AQ_Account_Holder_Flag__c = a.AQ_Account_Holder_Flag__c;
                  AccConRelToUpdates.AQ_Account_Nominee_customer_Id__c = a.AQ_Account_Nominee_customer_Id__c;
                  AccConRelToUpdates.AQ_Account_Nominee_Status_reason__c = a.AQ_Account_Nominee_Status_reason__c;
                  AccConRelToUpdates.AQ_Account_Nominee_User_Flag__c = a.AQ_Account_Nominee_User_Flag__c;
                  AccConRelToUpdates.AQ_Contact_Role__c = a.AQ_Contact_Role__c; 
                  AccConRelToUpdates.AQ_Key_Decision_Maker__c = a.AQ_Key_Decision_Maker__c;
                  AccConRelToUpdates.AQ_Key_Travel_Coordinator__c = a.AQ_Key_Travel_Coordinator__c;
                  AccConRelToUpdates.AQ_Membership_Number__c = a.AQ_Membership_Number__c;
                  AccConRelToUpdates.AQ_Promotions__c = a.AQ_Promotions__c; 
                  AccConRelToUpdates.AQ_Status__c = a.AQ_Status__c;
                  AccConRelToUpdates.AQ_Updates_Program_News__c = a.AQ_Updates_Program_News__c;
                  AccConRelToUpdates.AQ_Frequent_Flyer_Number__c = a.AQ_Frequent_Flyer_Number__c;
                  AccConRelToUpdates.AQ_Points_Balance_Account_Information__c = a.AQ_Points_Balance_Account_Information__c;
                  
       AccountContactRelation acr = [select id,AQ_Airline_News_and_Promotions__c,AQ_ABN__c,AQ_Salutation__c,AQ_First_Name__c,AQ_Last_Name__c,AQ_Email__c,AQ_Phone__c,AQ_Job_Title__c,Related_Email__c,Related_Phone__c,AQ_Account_Nominee_Air_Traveller_Status__c,AQ_Account_Nominee_customer_Id__c,AQ_Account_Holder_Flag__c,AQ_Account_Nominee_Status_reason__c,AQ_Account_Nominee_User_Flag__c,AQ_Contact_Role__c,AQ_Key_Decision_Maker__c,AQ_Key_Travel_Coordinator__c,AQ_Membership_Number__c,AQ_Promotions__c,AQ_Status__c,AQ_Updates_Program_News__c,AQ_Frequent_Flyer_Number__c,AQ_Points_Balance_Account_Information__c from AccountContactRelation where id =:a.Staging_ID__c];
           system.debug('Accconrelationlist'+ acr); 
           
               AccConRelToUpdates.Id = acr.Id;
               System.debug('AccconrelUpdatesID'+ AccConRelToUpdates.Id);          
               AccConRelToUpdates1.add(AccConRelToUpdates); 
               System.debug('###Updaterecords###'+AccConRelToUpdates1);
              } 
           }
     
           try {
                 //  Database.Insert(AccConRelToInsert);
                 // Database.Update(AccConRelToUpdates1);      
                 Database.SaveResult[] srListInsert = Database.Insert(AccConRelToInsert, false);
  for (Database.SaveResult sr : srListInsert ) {
        if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully Inserted account contact relationship:' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('account contact relationship fields that affected this error: ' + err.getFields());
        }
    }
}
           Database.SaveResult[] srListUpdate = Database.Update(AccConRelToUpdates1, false);
  for (Database.SaveResult sr : srListUpdate) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully Updated account contact relationship:' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('account contact relationship fields that affected this error: ' + err.getFields());
        }
      }
    }                
}
               
            catch(Exception e){
               System.debug('Exception Occured: '+e.getMessage());   
            }
        
       }
  global void finish(Database.BatchableContext bc) {
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];//get the job Id
      System.debug('$$$ Jobid is'+BC.getJobId());
      mail.setToAddresses(email);
      mail.setReplyTo('priyadharshinivellingiri@qantas.com.au');
      mail.setSenderDisplayName('Apex Batch Processing Module');
      mail.setSubject('Batch Processing '+a.Status);
      mail.setPlainTextBody('The Batch Apex job processed  '+a.TotalJobItems+'batches with  '+a.NumberOfErrors+'failures'+'Job Item processed are'+a.JobItemsProcessed);
      Messaging.sendEmail(new Messaging.Singleemailmessage [] {mail});
         // Delete Staging Account Relationship Data's  
      /*   for(Staging_Account_Contact_Relationship__c stgAcr : allACR){
               deleteACRList.add(stgAcr);
               System.debug('###Deleterecords###'+deleteACRList);
            }
            
            try{
                Database.Delete(deleteACRList);
            }catch(Exception e){
                System.debug('Exception Occured : '+e.getMessage());
            } */
        } 
    }