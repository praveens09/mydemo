@isTest
private class JwtTest {
     
    // No API for createing a certificate so must be manually pre-created
    // using Setup -> Security Controls -> Certificate and Key Management
    private static final String PRE_CREATED_CERTIFICATE_NAME = 'Qantas_Pre_Prod_FY17_1';
    private static final String FAKE_TOKEN = 'fakeToken';
     
    private class Mock implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            System.assertEquals('POST', req.getMethod());
            System.assert(req.getBody().contains('grant_type'), req.getBody());
            System.assert(req.getBody().contains('assertion'), req.getBody());
             
            res.setStatusCode(200);
            res.setBody('{"scope":"api","access_token":"' + FAKE_TOKEN + '"}');
 			system.debug('The value of res: ' + String.valueOf(res));
            return res;
        }
    }
 
    @isTest
    static void test() {
         
        Jwt.Configuration config = new Jwt.Configuration();
        config.jwtUsername = 'michael.e.sharp@capgemini.com.preprod';
        config.jwtSigningCertificateName = PRE_CREATED_CERTIFICATE_NAME;
        config.jwtHostname = 'qantas--Preprod.cs31.my.salesforce.com';
        config.jwtConnectedAppConsumerKey = '3MVG9Se4BnchkASnbifzy8UWzI59kuChwUIf1dz686.alg6MX_dKvAdyLPCEG937doUHyVkwHYR2eDduhA58P';
         
        Test.setMock(HttpCalloutMock.class, new Mock());
        Test.startTest();
        String accessToken = new Jwt(config).requestAccessToken();
        Test.stopTest();
        System.assertEquals(FAKE_TOKEN, accessToken);
        system.debug('YOUR TOKEN IS: ' + String.valueOf(accessToken));
    }
}