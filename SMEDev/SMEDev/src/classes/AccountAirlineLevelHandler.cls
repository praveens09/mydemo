public class AccountAirlineLevelHandler {
    
    public static boolean isRun = false;
        
    public static void updateDiscountCodeOnProducts(Map<Id,Account> newMapAccount,Map<Id,Account> oldMapAccount){
        set<Id> updatedAccountIds = new set<Id>();
        for(Account acc: newMapAccount.values()){
            if(acc.Airline_Level__c != oldMapAccount.get(acc.id).Airline_Level__c || acc.Airline_Level__c != null){
                updatedAccountIds.add(acc.id);
            }
            if(acc.Airline_Level__c != null){
                QBD_Online_Office_ID__c officeID = QBD_Online_Office_ID__c.getValues(acc.Airline_Level__c);
                if(officeID != null){               
                    acc.key_Contact_Office_ID__c = officeID.Key_Contact_Office_ID__c;
                }
            }
        }
        if(updatedAccountIds.isEmpty()) return;
        system.debug('Updateing Products');
        list<Product_Registration__c> accountProducts = [SELECT Id, Key_Contact_Office_ID__c, Active__c,Account__r.Airline_Level__c,Account__c FROM Product_Registration__c where type__c='QBD' and Account__c in:updatedAccountIds];
        list<Product_Registration__c> updatePR = new list<Product_Registration__c>(); 
        for(Product_Registration__c productReg : accountProducts){
            Account acc = newMapAccount.get(productReg.Account__c);
            if(acc.Airline_Level__c != null){
                QBD_Online_Office_ID__c officeID = QBD_Online_Office_ID__c.getValues(acc.Airline_Level__c);
                if(officeID != null){
                    if( productReg.Key_Contact_Office_ID__c != officeID.Key_Contact_Office_ID__c){
                        productReg.Key_Contact_Office_ID__c = officeID.Key_Contact_Office_ID__c;
                        updatePR.add(productReg);
                        newMapAccount.get(productReg.Account__c).Process_QBD_Update__c = false;
                    }
                }
            }
        }
        if(updatePR.size() > 0){
            update updatePR;
        }
        isRun = true;
    }
}