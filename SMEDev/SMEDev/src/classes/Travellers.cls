/*------------------------------------------------------------
Author:        M. Sharp
Company:       Capgemini
Description:   Proof of Concept
Inputs:
Test Class:
History
17-Oct-2016   M. Sharp     Initial Design 

------------------------------------------------------------*/

@RestResource(urlMapping = '/travellers/*')

global with sharing class Travellers {
	
	global class ApiError{
		string message {get;set;}
		string errorCode {get;set;}
		ApiError(string m, string e){
			this.message = m;
			this.errorCode = e; 
		}
	}
	
    @HttpGet
    global static void doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        RestContext.response.addHeader('Content-Type', 'application/json');
        Boolean isException = false;
        String abn = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);
        try{
            List<TravellerWrapper> lstTravellerWrap = new List<TravellerWrapper>();
            //Get all members where QBD = true, Account is active with QBD/QBR product.
            Account acc = getValidAccountId(abn);
            if (acc != null) {
                if(!acc.Aquire_System__c){            
                    res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoDataFoundCode'));
                    res.responseBody = Blob.valueOf(JSON.serializePretty(new ApiError(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoABNResponseBody'),'salesforce.travellers.abn.unregistered')));
                }else{
                    for(Associated_Person__c objAsoPerson: [SELECT id, Frequent_Flyer_Number__c, Last_Name__c, First_Name__c, 
                                                            Salutation__c FROM Associated_Person__c WHERE Active_QBD_Record__c = true and Account__c =:acc.Id and Frequent_Flyer_Number__c != '' ]){
                        lstTravellerWrap.add(new TravellerWrapper(objAsoPerson));
                    }
                    res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_GetSucessCode'));
                    RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(new TravellerResponseWrapper(!lstTravellerWrap.isEmpty() ? lstTravellerWrap : new List<TravellerWrapper>())));
                }
            }
            else {
                res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoDataFoundCode'));
                res.responseBody = Blob.valueOf(JSON.serializePretty(new ApiError(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoDataResponseBody'),'salesforce.travellers.abn.invalid')));
            }
        } 
        catch (Exception ex) {
            isException = true;
            //res.responseBody = Blob.valueOf(String.valueOf(ex) + '\n\n' + ex.getStackTraceString());
            res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_GetExceptionCode'));
            List<String> lstErrMsg = new List<String>();
            lstErrMsg.add('Traveller Info Rest Service - HttpGet');
            lstErrMsg.add('travellers');
            lstErrMsg.add('doGet');
            lstErrMsg.add(UserInfo.getUserName());
            Map<String, String> mapMsg= new Map<String, String>();
            mapMsg.put('Inbound', String.valueOf(req));
            mapMsg.put('Outbound', String.valueOf(res));
            CreateLogs.createApplicationLog('', ex, lstErrMsg, mapMsg);
        }
        finally{
            List<Integration_Logs__c> lstIntegrationLog = new List<Integration_Logs__c>();
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('Traveller HttpGet',String.valueOf(req),'Inbound Service','' ,isException));
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('Traveller HttpGet',String.valueOf(res),'Outbound Service','' ,isException));
            insert lstIntegrationLog;
        }
    }

    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String abn = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);
        String jsonString = RestContext.request.requestBody.toString();
        system.debug('JSON STRING: ' + jsonString);
        Boolean isException = false;
        try {
            Account acc = getValidAccountId(abn);
            list<Associated_Person__c> aprList = new list<Associated_Person__c>();
            if (acc != null) {
                if(!acc.Aquire_System__c){            
                    res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoDataFoundCode'));
                    res.responseBody = Blob.valueOf(JSON.serializePretty(new ApiError(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoABNResponseBody'),'salesforce.travellers.abn.unregistered')));
                }else{
	                // Parse entire JSON response.
	                JSONParser parser = JSON.createParser(jsonString);
	                while (parser.nextToken() != null) {
	                    // Start at the array of travellers.
	                    if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
	                        while (parser.nextToken() != null) {
	                            // Advance to the start object marker to find next traveller object.
	                            if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
	                                // Read entire traveller object
	                                TravellerWrapper t = (TravellerWrapper)parser.readValueAs(TravellerWrapper.class);

	                                //Build and upsert Associated_Person__c record
	                                Associated_Person__c apr   = new Associated_Person__c();
	                                apr.First_Name__c = t.firstName;
	                                apr.Last_Name__c = t.lastName;
	                                apr.Frequent_Flyer_Number__c = t.ffNumber;
	                                apr.Salutation__c = t.title;
	                                apr.Account__c = acc.id;
									apr.Active_QBD_Record__c = true;
									apr.start_date__c = system.today();
	                                aprList.add(apr);
	                                parser.skipChildren();
	                            }
	                        }
	                    }
	                }
	                insert aprList;
	                res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_GetSucessCode'));	
                }
            }
            else {
                res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoDataFoundCode'));
                res.responseBody = Blob.valueOf(JSON.serializePretty(new ApiError(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoDataResponseBody'),'salesforce.travellers.abn.invalid')));
            }
        } 
        catch (Exception ex) {
            isException = true;
            res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_PostExceptionCode'));
            List<String> lstErrMsg = new List<String>();
            lstErrMsg.add('Traveller Info Rest Service - HttpPost');
            lstErrMsg.add('travellers');
            lstErrMsg.add('doPost');
            lstErrMsg.add(UserInfo.getUserName());
            Map<String, String> mapMsg= new Map<String, String>();
            mapMsg.put('Inbound', String.valueOf(req));
            mapMsg.put('Outbound', String.valueOf(res));
            CreateLogs.createApplicationLog('', ex, lstErrMsg, mapMsg);
        }
        finally{
            List<Integration_Logs__c> lstIntegrationLog = new List<Integration_Logs__c>();
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('Traveller HttpGet',String.valueOf(req),'Inbound Service','' ,isException));
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('Traveller HttpGet',String.valueOf(res),'Outbound Service','' ,isException));
            insert lstIntegrationLog;
        }

    }

    @HttpPatch
    global static void doPatch() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String abn = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);
        system.debug('abn#######'+abn);
        String jsonReqBodyString = RestContext.request.requestBody.toString();
        system.debug('JSON STRING: ' + jsonReqBodyString);
        Boolean isException = false;
        Savepoint sp = Database.setSavePoint(); 
        try {
            list<String> ffNumbers = new list<String>(); 
            list<Associated_Person__c> assoMembers = new List<Associated_Person__c>();

            // Parse entire JSON request.
            JSONParser parser = JSON.createParser(jsonReqBodyString);
            while (parser.nextToken() != null) {
                // Start at the array of travellers.
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                    while (parser.nextToken() != null) {
                        // Advance to the start object marker to find next traveller object.
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            TravellerWrapper t = (TravellerWrapper)parser.readValueAs(TravellerWrapper.class);  // Read entire traveller object
                            ffNumbers.add(t.ffNumber);
                            parser.skipChildren();      // Skip the child start array and start object markers.
                        }
                    }
                }
            }
            
            //Query the FF numbers
            Account acc = getValidAccountId(abn);
            if (acc != null) {
                if(!acc.Aquire_System__c){            
                    res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoDataFoundCode'));
                    res.responseBody = Blob.valueOf(JSON.serializePretty(new ApiError(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoABNResponseBody'),'salesforce.travellers.abn.unregistered')));
                }else{            
		            list<Associated_Person__c> memberList = [SELECT id, Account__c,  End_Date__c, Frequent_Flyer_Number__c, Active_QBD_Record__c FROM Associated_Person__c 
		            WHERE Account__r.ABN_Tax_Reference__c =: abn
		            AND Account__r.Active__c = true
		            AND Frequent_Flyer_Number__c != '' 
		            AND Frequent_Flyer_Number__c in: ffNumbers];
		
		            for(Associated_Person__c member : memberList){
		                if(member.Active_QBD_Record__c){
		                    member.Active_QBD_Record__c = false;
		                    member.End_Date__c = Date.today();
		                    assoMembers.add(member); 
		                }
		            }
		            system.debug('memberList######'+memberList.size());
		            system.debug('ffNumbers#######'+ffNumbers.size());
		            if(memberList.size() != ffNumbers.size()){
		                res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostNoDataFoundCode'));
		            	res.responseBody = Blob.valueOf(JSON.serializePretty(new ApiError(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostNoDataResponseBody'),'salesforce.travellers.delete.invalidTravellers')));
		            }else if(!assoMembers.isEmpty()){ 
		                update assoMembers;          
		                res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostSucessCode'));
		            }   
                }  
            } else {
                res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoDataFoundCode'));
                res.responseBody = Blob.valueOf(JSON.serializePretty(new ApiError(CustomSettingsUtilities.getConfigDataMap('Traveller_GetNoDataResponseBody'),'salesforce.travellers.abn.invalid')));
            }   
        } 
        catch(Exception ex){
            Database.rollback(sp);
            isException = true;
            res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostExceptionCode'));
			res.responseBody = Blob.valueOf(JSON.serializePretty(new ApiError(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostExceptionBody'),'salesforce.travellers.')));            
            List<String> lstErrMsg = new List<String>();
            lstErrMsg.add('Delete Traveller Info Rest Service - HttpPatch');
            lstErrMsg.add('Deletetravellers');
            lstErrMsg.add('doPatch');
            lstErrMsg.add(UserInfo.getUserName());
            Map<String, String> mapMsg= new Map<String, String>();
            mapMsg.put('Inbound', String.valueOf(req));
            mapMsg.put('Outbound', String.valueOf(res));
            CreateLogs.createApplicationLog('', ex, lstErrMsg, mapMsg);
        }
        finally{
            List<Integration_Logs__c> lstIntegrationLog = new List<Integration_Logs__c>();
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('Delete Traveller HttpPatch',String.valueOf(req),'Inbound Service','' ,isException));
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('Delete Traveller HttpPatch',String.valueOf(res),'Outbound Service','' ,isException));
            insert lstIntegrationLog;
        }
    }    
    
    global static Account getValidAccountId(String abn){
        Account acc;
        try{
           acc = [select id,Aquire_System__c,QBD__c from Account where ABN_Tax_Reference__c =: abn and Active__c = true limit 1];   
        }catch(Exception e){
        
        }
        return (acc == null)? null: acc;
    }

}