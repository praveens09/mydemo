/*==================================================================================================================================
Author:         Praveen Sampath
Company:        Capgemini
Purpose:        Test Class for CreateQBRRegistration Class
Date:           25/10/2016          
History:

==================================================================================================================================

==================================================================================================================================*/

@isTest(SeeAllData = false)
private class DeleteTravellersTest{
	@testSetup
	static void createTestData(){
		
	}
	static TestMethod void deleteDoPostTest(){
		String reqBody = '{	"travellers": [ {	"ffNumber": "6142107","lastName": "Plum","firstName": "Victoria","title": "Mrs"},';
		reqBody += ' {"ffNumber": "6142104","lastName": "Fysh","firstName": "Victoria","title": "Ms"  }]}';

		RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();

	    // pass the req and resp objects to the method     
	    req.requestURI = '/services/apexrest/Deletetravellers';  
	    req.httpMethod = 'POST';
	    req.requestBody = blob.valueOf(reqBody);
	    RestContext.request = req;
	    DeleteTravellers.doPost();

	}
}