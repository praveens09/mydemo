public class testtravellers{
public static void run(){

savepoint sp = Database.setSavepoint();
Account acc = new Account();
acc.Name = 'TestCompany';
acc.ABN_Tax_Reference__c  = '11223344556';
acc.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
insert acc;

Product_Registration__c qbdProduct = new Product_Registration__c();
qbdProduct.RecordTypeId =  Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get('QBD Registration').getRecordTypeId();
qbdProduct.Account__c = acc.Id;
qbdProduct.name = acc.Name;
insert qbdProduct;

Associated_Person__c member1 = new Associated_Person__c();
member1.Last_Name__c = 'testlastname1';
member1.Frequent_Flyer_Number__c = '9988776655';
member1.Active_QBD_Record__c = true;
member1.QBD_Travel_Arranger__c = 'N';
member1.QBD_First_Name__c = 'fname1';
member1.QBD_Last_Name__c = 'lname1';
member1.Account__c = acc.Id;
member1.Start_Date__c = system.today();
insert member1;

Associated_Person__c member2 = new Associated_Person__c();
member2.Last_Name__c = 'testlastname2';
member2.Frequent_Flyer_Number__c = '998877665544';
member2.Active_QBD_Record__c = true;
member2.QBD_Travel_Arranger__c = 'N';
member2.QBD_First_Name__c = 'fname2';
member2.QBD_Last_Name__c = 'lname2';
member2.Account__c = acc.Id;
member2.Start_Date__c = system.today();
insert member2;

RestRequest req = new RestRequest(); 
req.requestURI = '/services/apexrest/Travellers/'+acc.ABN_Tax_Reference__c;  
req.httpMethod = 'GET';
RestContext.request = req;
RestResponse res = new RestResponse();
RestContext.response = res;
Travellers.doGet();
system.debug('GET_STATUS: '+res.statusCode);
if(res.responseBody != null){
system.debug('GET_RES_BODY: '+((TravellerResponseWrapper)JSON.deserialize(res.responseBody.toString(), TravellerResponseWrapper.class)).travellers);
}

RestRequest req2 = new RestRequest(); 
req2.requestURI = '/services/apexrest/Travellers/'+acc.ABN_Tax_Reference__c;  
req2.httpMethod = 'PATCH';
req2.requestBody = res.responseBody;
RestContext.request = req2;
RestResponse res2 = new RestResponse();
RestContext.response = res2;
Travellers.doPatch();
system.debug('PATCH_STATUS: '+res2.statusCode);

RestRequest req3 = new RestRequest(); 
req3.requestURI = '/services/apexrest/Travellers/'+acc.ABN_Tax_Reference__c;  
req3.httpMethod = 'GET';
RestContext.request = req3;
RestResponse res3 = new RestResponse();
RestContext.response = res3;
Travellers.doGet();
system.debug('GET_STATUS: '+res3.statusCode);
if(res3.responseBody != null){
system.debug('GET_RES_BODY: '+res3.responseBody.toString());
}

Database.rollback(sp);
}
}