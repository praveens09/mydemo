/*------------------------------------------------------------
Author:        M. Sharp
Company:       Capgemini
Description:   REST Proof of Concept - Do not modify this class any further.
Inputs:        Mapping per 'Aquire File Structure v3.xlsx'
Test Class:
History
28-Sept-2016   M. Sharp     Initial Design
10-Oct-2016    p. Douglas   GSTregistered data type changed to string
------------------------------------------------------------*/

@RestResource(urlMapping = '/QBRRegistration/*')
global with sharing class QBRRegistration {

	@HttpPut

	global static void putQBRRegistration(
	    String abn,
	    String entityName,
	    String numberOfEmployees,
	    String rewardTier,
	    String gstRegistered,

	    String companyName,
	    String membershipNumber,
	    String stageStatus,
	    String entityType,

	    String businessType,
	    String jobRole,
	    String function,
	    String firstName,
	    String lastName,
	    String frequentFlyerNumber


	) {

		//Query used to find if there are duplicate ABNs in Salesforce
		List<Account> abnq = [SELECT Id, ABN_Tax_Reference__c, RecordType.name, Aquire_Override__c, AMEX_response_code__c FROM Account WHERE ABN_Tax_Reference__c = :abn];

		Integer recordCount = abnq.size();
		system.debug('Record Count: ' + String.valueOf(recordCount));

		//Store results of SOQL query in a map and get counts of all of the record types
		Map<String, Integer> theCounts = new Map<String, Integer>();

		for (Account acc : abnq) {
			string key = acc.RecordType.name;
			if (!theCounts.containsKey(key)) {
				theCounts.put(key, 0);
			}
			Integer currentInt = theCounts.get(key) + 1;
			theCounts.put(key, currentInt);
		}
		system.debug('COUNT IS: ' + String.valueOf(theCounts));

		if (theCounts.containsKey('Prospect Account') && theCounts.get('Prospect Account') == 2) {
			system.debug('There are 2 Prospect Accounts');
		} else {
			system.debug('Else statement reached');
		}

		//Get Account RecordTypeId
		Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();

		//Get Contact RecordTypeId
		Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contact').getRecordTypeId();

		//Get Product Registration RecordTypeId
		Id prRecordTypeId = Schema.SObjectType.Product_Registration__c.getRecordTypeInfosByName().get('Aquire Registration').getRecordTypeId();


		//Build and upsert account record
		Account acc = new Account();
		acc.ABN_Tax_Reference__c = abn;
		acc.Name = entityName;
		acc.Acquire_Number_of_employees__c = numberOfEmployees;
		//acc.QBR_Reward_Tier__c = rewardTier;
		acc.GST_Registered__c = gstRegistered;
		acc.Aquire_Membership_Number__c = membershipNumber;
		acc.RecordTypeId = accRecordTypeId;
		acc.Aquire_Override__c = 'N';
		upsert acc  Aquire_Membership_Number__c;


		//Build and upsert Product Registration record
		Product_Registration__c pr = new Product_Registration__c();
		pr.Name = companyName;
		pr.Membership_Number__c = membershipNumber;
		pr.Stage_Status__c = stageStatus;
		pr.Aquire_Entity_Name__c = entityName;
		pr.Aquire_Entity_Type__c = entityType;
		pr.RecordTypeId = prRecordTypeId;
		//Does ABN already exist in Salesforce?  If so then link the existing account id to the Product Registration.
		if (abnq.size() > 0) {
			pr.Account__c = abnq[0].id;
		} else {
			pr.Account__c = acc.id;
		}
		upsert pr Membership_Number__c;

		//Build and upsert contact record
		Contact con = new Contact();
		con.Business_Types__c = businessType;
		con.Job_Role__c = jobRole;
		con.Function__c = function;
		con.FirstName = firstName;
		con.LastName = lastName;
		con.Frequent_Flyer_Number__c = frequentFlyerNumber;
		con.AccountId = acc.id;
		con.RecordTypeId = conRecordTypeId;
		//con.Active__c = false;
		upsert con Frequent_Flyer_Number__c;

		//Build and upsert Associated_Person__c record
		Associated_Person__c apr = new Associated_Person__c();
		apr.First_Name__c = firstName;
		apr.Last_Name__c = lastName;
		apr.Frequent_Flyer_Number__c = frequentFlyerNumber;
		apr.Account__c = acc.id;
		apr.Contact__c = con.id;
		upsert apr Frequent_Flyer_Number__c;


		//Build out JSON response
		Map<String, String> m = new Map<String, String>(); // Define a new map and add output fields to JSON
		m.put('sourceCode', 'QUEST');
		m.put('batchDate', string.valueOfGmt(System.now()));
		m.put('membershipNumber', membershipNumber);
		//m.put('smeOverride', string.valueOf(abnq[0].Aquire_Override__c));
		//m.put('amexEligibility', string.valueOf(abnq[0].AMEX_response_code__c));

		RestContext.response.addHeader('Content-Type', 'application/json');
		RestContext.response.responseBody = Blob.valueOf(JSON.serialize(m));

	}
}