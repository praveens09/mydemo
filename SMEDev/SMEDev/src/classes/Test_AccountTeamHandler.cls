@isTest
public class Test_AccountTeamHandler {

    public static testMethod void myTest(){
        
        List<Account> accList = new List<Account>();
        List<Product_Registration__c> prodList = new List<Product_Registration__c>();
        List<AccountTeamMember> accTeamList = new List<AccountTeamMember>();        
        List<Account_Owner__c> accOwnerList = new List<Account_Owner__c>();
        Integer r = 3000;
        
        // Custom setting creation
        TestUtilityDataClassQantas.enableTriggers();
        TestUtilityDataClassQantas.getAccountRevenueCSetting();        
        
        // Account Owner object data creation    
        Account_Owner__c ao = new Account_Owner__c(Name = 'S', Account_Manager__c = UserInfo.getUserId());        
        
        accOwnerList.add(ao);
        
        try{
            Database.Insert(accOwnerList);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        }       
        
        //Accounts        
        accList = TestUtilityDataClassQantas.getAccounts();
        
        // AccountTeamMember data creation        
        for(Account acc : accList){
            AccountTeamMember accTeamMember = new AccountTeamMember();
            accTeamMember.AccountId = acc.id;
            accTeamMember.UserId = UserInfo.getUserId();
            accTeamMember.TeamMemberRole = 'Account Manager';
            accTeamList.add(accTeamMember);
        }
        
        try{
            Database.Insert(accTeamList);
        }catch(Exception e){
            System.debug('Error Occured: '+e.getMessage());
        } 
        
        // Sample Product Registration Data Creation
        for(Account acc : accList){
            Product_Registration__c prod = new Product_Registration__c(Name = acc.Name, Account__c = acc.Id, Aq_Business_Domestic_annual_spend__c = '300000', 
                                                                       Aq_Business_international_annual_spend__c = '100000' , RecordTypeId = TestUtilityDataClassQantas.aquireRecordTypeId,
                                                                       Stage_Status__c = 'Active', Annual_Domestic_Flights__c = '300+'
                                                                       );
            prodList.add(prod);
        } 
        
        try{
            Database.Insert(prodList);            
        }catch(Exception e){
            System.debug('Error Occured1: '+e.getMessage());
        }  

        // Updation of Account Data
        for(Account acc : accList){                      
            acc.Estimated_Total_Air_Travel_Spend__c = r;
            r = r*10;           
        }
        
        try{
            Database.Update(accList);
        }catch(Exception e){
            System.debug('Error Occured while updating: '+e.getMessage());
        }
        
        // Updation of Account Data
        for(Account acc : accList){                      
            acc.Estimated_Total_Air_Travel_Spend__c = acc.Estimated_Total_Air_Travel_Spend__c + 1;
            acc.AMEX__c = true;
            acc.Aquire__c = false;
        }
        
        try{
            Database.Update(accList);
        }catch(Exception e){
            System.debug('Error Occured while updating: '+e.getMessage());
        }              
        
    }
}