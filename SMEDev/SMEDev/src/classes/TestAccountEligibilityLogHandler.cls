//CreatedBy 	: Gourav Bhardwaj
//Jira 			: 1787
//Description	: Test Class for AccountEligibilityLogHandler
/**********************************************************************************************************
Change History:
======================================================================================================================
Name                	Jira    	Description                                             	Tag
======================================================================================================================
Gourav Bhardwaj     	1821    	Test Class coverage for Corporate Deal						T01
********************************************************************************************************/
@isTest
public class TestAccountEligibilityLogHandler {
    //Aquire Override Flag Accounts
    private static Account account1;
    private static Account account2;
    private static Account account3;
    
    
    //Travel Agent Flag Accounts
    private static Account account5;
    private static Account account6;
    
    //Corporate Deal Flag Test Data -------- T01
    private static Account account7;
    private static Account account8;
    private static Opportunity opportunity;
    
    private static Contract__c contract;
    private static Contract__c contract1;
    private static Contract__c contract2;
    //T01 Ends
    
    public static Account_Eligibility_Log__c accountEligibility1;
    public static Account_Eligibility_Log__c accountEligibility2;
    public static Account_Eligibility_Log__c accountEligibility22;
    public static Account_Eligibility_Log__c accountEligibility3;
    public static Account_Eligibility_Log__c accountEligibility4;
    public static Account_Eligibility_Log__c accountEligibility44;
    
    static{
        TestUtilityDataClassQantas.enableTriggers();
        //Aquire Override test Data
        account1 = new Account();
        account1.Name = 'account1';
        account1.Aquire_Override__c = 'N';
        account1.Type = 'Prospect Account';
        account1.RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId;
        
        account2 = new Account();
        account2.Name = 'account2';
        account2.Aquire_Override__c = 'N';
        account2.Type = 'Prospect Account';
        account2.RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId;
        
        account3 = new Account();
        account3.Name = 'account3';
        account3.Aquire_Override__c = 'Y';
        account3.Aquire_Override_End_Date__c = Date.today().addDays(3);
        account3.Aquire_Override_Reason__c='Receiving third-party private airfares, discounts or rebates from Qantas';
        account3.Type = 'Prospect Account';
        account3.RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId;
        
        
        //Travel Agent Test Data
        account5 = new Account();
        account5.Name = 'account5';
        account5.Type = 'Prospect Account';
        account5.Industry = 'Mining';
        account5.RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId;
        
        account6 = new Account();
        account6.Name = 'account6';
        account6.Industry = 'Travel';
        account6.Type = 'Prospect Account';
        account6.RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId;
        
        //Corporate Deal Flag Test Data -------- T01
        account7 = new Account();
        account7.Name = 'account7';
        account7.Aquire_Override__c = 'N';
        account7.Type = 'Prospect Account';
        account7.Industry = 'Mining';
        account7.RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId;
        
        account8 = new Account();
        account8.Name = 'account8';
        account8.Aquire_Override__c = 'N';
        account8.Type = 'Prospect Account';
        account8.Industry = 'Mining';
        account8.RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId;        
        //T01 Ends
        
        insert new List<Account>{account1,account2,account3,account5,account6,account7,account8};
            
            //Travel Agent IntraDay data 
            accountEligibility1 = new Account_Eligibility_Log__c();
        accountEligibility1.FieldName__c = Schema.Account.fields.Agency__c.getDescribe().getLabel();
        accountEligibility1.Value__c = 'Y';
        accountEligibility1.StartDate__c = Date.today();
        accountEligibility1.EndDate__c = date.newinstance(3999,12,31);
        accountEligibility1.Account__c = account5.Id;
        
        //Travel Agent Previous day data
        accountEligibility2 = new Account_Eligibility_Log__c();
        accountEligibility2.FieldName__c = Schema.Account.fields.Agency__c.getDescribe().getLabel();
        accountEligibility2.Value__c = 'Y';
        accountEligibility2.StartDate__c = Date.today().addDays(-5);
        accountEligibility2.EndDate__c = date.newinstance(3999,12,31);
        accountEligibility2.Account__c = account5.Id;
        
        accountEligibility22 = new Account_Eligibility_Log__c();
        accountEligibility22.FieldName__c = Schema.Account.fields.Agency__c.getDescribe().getLabel();
        accountEligibility22.Value__c = 'Y';
        accountEligibility22.StartDate__c = Date.today().addDays(-7);
        accountEligibility22.EndDate__c = date.newinstance(3999,12,31);
        accountEligibility22.Account__c = account5.Id;
        
        //Aquire Override IntraDay data 
        accountEligibility3 = new Account_Eligibility_Log__c();
        accountEligibility3.FieldName__c = Schema.Account.fields.Aquire_Override__c.getDescribe().getLabel();
        accountEligibility3.Value__c = 'Y';
        accountEligibility3.StartDate__c = Date.today();
        accountEligibility3.EndDate__c = date.newinstance(3999,12,31);
        accountEligibility3.Account__c = account1.Id;
        
        //Aquire Override Previous day data
        accountEligibility4 = new Account_Eligibility_Log__c();
        accountEligibility4.FieldName__c = Schema.Account.fields.Aquire_Override__c.getDescribe().getLabel();
        accountEligibility4.Value__c = 'Y';
        accountEligibility4.StartDate__c = Date.today().addDays(-5);
        accountEligibility4.EndDate__c = date.newinstance(3999,12,31);
        accountEligibility4.Account__c = account2.Id;
        
        accountEligibility44 = new Account_Eligibility_Log__c();
        accountEligibility44.FieldName__c = Schema.Account.fields.Aquire_Override__c.getDescribe().getLabel();
        accountEligibility44.Value__c = 'Y';
        accountEligibility44.StartDate__c = Date.today().addDays(-7);
        accountEligibility44.EndDate__c = date.newinstance(3999,12,31);
        accountEligibility44.Account__c = account2.Id;
        
        insert new List<Account_Eligibility_Log__c> {accountEligibility1,accountEligibility2,accountEligibility22,accountEligibility3,accountEligibility4,accountEligibility44};
            
            
        //Corporate Deal Flag Test Data -------- T01
        
		Contract_Categories__c contractCategory1 = new Contract_Categories__c();
        contractCategory1.Name = 'Corporate Airfares';
       
        Contract_Categories__c contractCategory2 = new Contract_Categories__c();
        contractCategory2.Name = 'Qantas Business Savings';
       
        insert new List<Contract_Categories__c> {contractCategory1,contractCategory2} ;
        
        opportunity = new Opportunity();
        opportunity.Name = 'Test Opportunity';
        opportunity.CloseDate = Date.today().addDays(5);
        opportunity.StageName = 'Identify';
        
        insert new List<Opportunity>{opportunity}; 
            
        contract = new Contract__c();
        contract.Name = 'Test Contract';
        contract.Status__c = 'Signature Required by Customer';
        contract.Contract_Start_Date__c = Date.today().addDays(3);
        contract.Contract_End_Date__c = Date.today().addDays(10);
        contract.Active__c=True;
        contract.Opportunity__c=opportunity.Id;
        contract.Account__c = account7.Id;
        contract.Type__c = 'Qantas Business Savings';
        
        contract1 = new Contract__c();
        contract1.Name = 'Test Contract1';
        contract1.Status__c = 'Signature Required by Customer';
        contract1.Contract_Start_Date__c = Date.today().addDays(3);
        contract1.Contract_End_Date__c = Date.today().addDays(10);
        contract1.Active__c=True;
        contract1.Opportunity__c=opportunity.Id;
        contract1.Account__c = account8.Id;
        contract1.Type__c = 'Corporate Airfares';
        
        insert new List<Contract__c> {contract,contract1};
            
            
            //T01 Ends
            
            }
    
    //Test method for travel Agent flag 		
    static testmethod void  testLogAccountEligibilityAquireOverrideFlag(){
        account1.Aquire_Override__c = 'Y';
        account1.Aquire_Override_End_Date__c = Date.today().addDays(5);
        account1.Aquire_Override_Reason__c = 'Receiving third-party private airfares, discounts or rebates from Qantas';
        
        account2.Aquire_Override__c = 'Y';
        account2.Aquire_Override_End_Date__c = Date.today().addDays(5);
        account2.Aquire_Override_Reason__c = 'Receiving third-party private airfares, discounts or rebates from Qantas';
        
        account3.Aquire_Override__c = 'N';
        
        update new List<Account> {account1,account2,account3};
            
            Test.startTest();
        
        account2.Aquire_Override_End_Date__c = Date.today().addDays(7);
        update new List<Account> {account2};
            
            Test.stopTest();
        for(Account_Eligibility_Log__c tempAEH : [SELECT Value__c,EndDate__c,StartDate__c,FieldName__c,Account__c,Id FROM Account_Eligibility_Log__c WHERE Account__c IN(:account1.Id,:account2.Id,:account3.Id) AND FieldName__c='Travel Agent' AND Value__c='Y']){
            if(tempAEH.Account__c==account1.Id ){
                system.assertEquals('Y', tempAEH.Value__c);
                system.assertEquals(Date.today().addDays(5), tempAEH.EndDate__c);
            }
            
            if(tempAEH.Account__c==account2.Id){
                system.assertEquals('Y', tempAEH.Value__c);
                system.assertEquals(Date.today().addDays(5), tempAEH.EndDate__c);
            }
            
            if(tempAEH.Account__c==account3.Id){
                system.assertEquals('N', tempAEH.Value__c);
                system.assertEquals(date.newinstance(3999,12,31), tempAEH.EndDate__c);
            }
        }
        
    }
    
    //Test method for travel Agent flag 
    static testmethod void  testLogAccountEligibilityAgencyFlag(){
        account5.Industry = 'Travel';
        account6.Industry = 'Construction';
        
        Test.startTest();
        update new List<Account> {account5,account6};
            
            for(Account_Eligibility_Log__c tempAEH : [SELECT Value__c,EndDate__c,StartDate__c,FieldName__c,Account__c,Id FROM Account_Eligibility_Log__c WHERE Account__c IN(:account5.Id,:account6.Id) AND FieldName__c='Travel Agent' AND Value__c='Y']){
                if(tempAEH.Account__c==account5.Id ){
                    system.assertEquals('Y', tempAEH.Value__c);
                }
                
                if(tempAEH.Account__c==account6.Id){
                    system.assertEquals('N', tempAEH.Value__c);
                }
            }
        Test.stopTest();
        
    }
    
    //Test method for travel Corporate Deal 
    static testmethod void  testLogAccountEligibilityContracts(){
        contract.Status__c = 'Signed by Customer';
        contract1.Status__c = 'Signed by Customer';
        
        //To insert new records in Account Eligibility Log Object
        Update new List<Contract__c> {contract,contract1};
            
            Test.startTest();
        
        contract.Contract_Start_Date__c = Date.today().addDays(5);
        contract.Contract_End_Date__c = Date.today().addDays(12);
        
        contract1.Contract_Start_Date__c = Date.today().addDays(5);
        contract1.Contract_End_Date__c = Date.today().addDays(12);
        
        //To Update existing Contract records in Account Eligibility Log Object
        Update new List<Contract__c> {contract,contract1};
            Test.stopTest();
        
        for(Account_Eligibility_Log__c tempAEH : [SELECT Value__c,ContractAndAgreement__c,EndDate__c,StartDate__c,FieldName__c,Account__c,Id FROM Account_Eligibility_Log__c WHERE ContractAndAgreement__c IN(:contract.Id,:contract1.Id) AND FieldName__c='Corporate Deal']){
            system.debug('### Eligibility Log Data : '+tempAEH);
            if(tempAEH.ContractAndAgreement__c==contract.Id || tempAEH.ContractAndAgreement__c==contract1.Id){
                system.assertEquals('Corporate Deal', tempAEH.FieldName__c);
            }
        }
    }
}