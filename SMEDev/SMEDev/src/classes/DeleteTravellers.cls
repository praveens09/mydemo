/*==================================================================================================================================
Author:         Praveen Sampath
Company:        Capgemini
Purpose:        To remove Associate link from the Account
Date:           28/10/2016          
History:

==================================================================================================================================

==================================================================================================================================*/

@RestResource(urlMapping = '/Deletetravellers/*')
global with sharing  class DeleteTravellers {

    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        String abn = req.requestURI.substring(req.requestURI.lastIndexOf('/') + 1);
        String jsonReqBodyString = RestContext.request.requestBody.toString();
        system.debug('JSON STRING: ' + jsonReqBodyString);
        Boolean isException = false;
        Savepoint sp = Database.setSavePoint(); 
        try {
            list<String> ffNumbers = new list<String>(); 
            list<Associated_Person__c> assoMembers = new List<Associated_Person__c>();

            // Parse entire JSON request.
            JSONParser parser = JSON.createParser(jsonReqBodyString);
            while (parser.nextToken() != null) {
                // Start at the array of travellers.
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                    while (parser.nextToken() != null) {
                        // Advance to the start object marker to find next traveller object.
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            TravellerWrapper t = (TravellerWrapper)parser.readValueAs(TravellerWrapper.class);  // Read entire traveller object
                            ffNumbers.add(t.ffNumber);
                            parser.skipChildren();      // Skip the child start array and start object markers.
                        }
                    }
                }
            }
            
            //Query the FF numbers
            list<Associated_Person__c> memberList = [SELECT id, Account__c, QBD_Travel_Arranger__c, End_Date__c, Frequent_Flyer_Number__c, Active_QBD_Record__c FROM Associated_Person__c 
            WHERE Account__r.ABN_Tax_Reference__c =: abn
            AND Frequent_Flyer_Number__c != '' 
            AND Frequent_Flyer_Number__c in: ffNumbers];
            for(Associated_Person__c member : memberList){
                if(member.Active_QBD_Record__c){
                    if(member.QBD_Travel_Arranger__c == 'Y'){
                        //Associated_Person__c qbdTraveller = member.clone(false, false);
                        //qbdTraveller.Frequent_Flyer_Number__c = '';
                        //assoMembers.add(qbdTraveller);                      
                        //member.QBD_Travel_Arranger__c = 'N';
                    }else{           
                        member.Active_QBD_Record__c = false;
                    }
                    member.End_Date__c = Date.today();
                    assoMembers.add(member);   
                }
            }
            if(memberList.size() != ffNumbers.size()){
                res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostNoDataFoundCode'));
                res.responseBody = Blob.valueOf(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostNoDataResponseBody'));
            }else if(!assoMembers.isEmpty()){ 
                update assoMembers;          
                res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostSucessCode'));
            }         
        } 
        catch(Exception ex){
            Database.rollback(sp);
            isException = true;
            //res.responseBody = Blob.valueOf(String.valueOf(ex) + '\n\n' + ex.getStackTraceString());
            res.statusCode = Integer.valueOf(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostExceptionCode'));
            res.responseBody = Blob.valueOf(CustomSettingsUtilities.getConfigDataMap('DeleteTraveller_PostExceptionBody'));
            List<String> lstErrMsg = new List<String>();
            lstErrMsg.add('Delete Traveller Info Rest Service - HttpPost');
            lstErrMsg.add('Deletetravellers');
            lstErrMsg.add('doPost');
            lstErrMsg.add(UserInfo.getUserName());
            Map<String, String> mapMsg= new Map<String, String>();
            mapMsg.put('Inbound', String.valueOf(req));
            mapMsg.put('Outbound', String.valueOf(res));
            CreateLogs.createApplicationLog('', ex, lstErrMsg, mapMsg);
        }
        finally{
            List<Integration_Logs__c> lstIntegrationLog = new List<Integration_Logs__c>();
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('Delete Traveller HttpPost',String.valueOf(req),'Inbound Service','' ,isException));
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('Delete Traveller HttpPost',String.valueOf(res),'Outbound Service','' ,isException));
            insert lstIntegrationLog;
        }
    }

    /*global static Id createCaseFromExcep(String accountId,string jsonReqBodyString){
        //TODO get Case Details
        Case qbdCase = new Case();
        //qbdCase.AccountId = accountId;              
        qbdCase.Status = 'Open';
        qbdCase.Reason = 'QBD Auto-closure';
        qbdCase.Origin = 'QBD Form';
        qbdCase.Priority = 'Medium';
        qbdCase.Type = '';
        qbdCase.Sub_Type__c = '';
        qbdCase.Subject = '';
        qbdCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('QBD Request').getRecordTypeId();
        qbdCase.Description = accountId+' '+jsonReqBodyString;
        for(Group g : [SELECT Id FROM Group WHERE name = 'QBD Request Queue']){
            qbdCase.OwnerId = g.id;
        }
        try {
            insert qbdCase;
        } catch (System.DmlException e){
            System.debug('ERROR: Not able to create Case: ' + e);
        }
        return qbdCase.id;
    } */  
}