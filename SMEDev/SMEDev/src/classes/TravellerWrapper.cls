//Wrapper class maps Associate person fields in the form requested
public class TravellerWrapper {
    public string ffNumber;
    public string lastName;
    public string firstName;
    public string title;
    public TravellerWrapper(Associated_Person__c objAsoPerson){
        this.ffNumber = objAsoPerson.Frequent_Flyer_Number__c;
        this.lastName = objAsoPerson.Last_Name__c;
        this.firstName = objAsoPerson.First_Name__c;
        this.title = objAsoPerson.Salutation__c;
    }
}