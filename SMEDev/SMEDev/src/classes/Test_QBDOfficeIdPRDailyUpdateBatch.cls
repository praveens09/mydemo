/**
 * This class contains unit tests for validating the behavior of QBDOfficeId_PR_Daily_Update_Batch calss
 * 
 */
@isTest
private class Test_QBDOfficeIdPRDailyUpdateBatch {

    static testMethod void testBatchProcess() {
       
        TestUtilityDataClassQantas.enableTriggers();
        
        insert new QBD_Online_Office_ID__c(Name = 'QBD', Key_Contact_Office_ID__c = '12345');
        insert new Account_Travel_Frequency__c(Name = 'Travel Frequency', Travel_Frequency__c = '10-20');
       
        List<Account> accList = new List<Account>();
        accList.add(new Account(Name = 'Sample1', Active__c = true, Aquire__c = true, Process_QBD_Update__c = true,
                                  RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId, Aquire_Blocking_Classification__c = false,
                                  Agency__c = 'N', Dealing_Flag__c = 'N', Aquire_Override__c = 'N' 
                                  ));
        accList.add(new Account(Name = 'Sample2', Active__c = true, Aquire__c = true, Process_QBD_Update__c = true, 
                                  RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId, Aquire_Blocking_Classification__c = false,
                                  Agency__c = 'N', Dealing_Flag__c = 'Y', Aquire_Override__c = 'N' 
                                  )); 
        accList.add(new Account(Name = 'Sample3', Active__c = true, Aquire__c = false, Process_QBD_Update__c = true, 
                                  RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId, Aquire_Blocking_Classification__c = false,
                                  Agency__c = 'N', Dealing_Flag__c = 'Y', Aquire_Override__c = 'N' 
                                  ));
                                   
        accList.add(new Account(Name = 'Sample4', Active__c = true, Aquire__c = false, Process_QBD_Update__c = true, 
                                  RecordTypeId = TestUtilityDataClassQantas.prospectRecordTypeId, Aquire_Blocking_Classification__c = false,
                                  Agency__c = 'N', Dealing_Flag__c = 'Y', Aquire_Override__c = 'N' 
                                  ));
        insert accList;
        
        List<Product_Registration__c> prList = new List<Product_Registration__c>(); 
        prList.add(new Product_Registration__c(Name = 'pr1', Account__c = accList[0].id, RecordTypeId = TestUtilityDataClassQantas.qbdRecordTypeId, Active__c = true));
        prList.add(new Product_Registration__c(Name = 'pr12', Account__c = accList[0].id, RecordTypeId = TestUtilityDataClassQantas.aquireRecordTypeId, Active__c = true, Stage_Status__c = 'Active'));
        prList.add(new Product_Registration__c(Name = 'pr1', Account__c = accList[1].id, RecordTypeId = TestUtilityDataClassQantas.qbdRecordTypeId, Active__c = true));
        prList.add(new Product_Registration__c(Name = 'pr21', Account__c = accList[1].id, RecordTypeId = TestUtilityDataClassQantas.aquireRecordTypeId, Active__c = true, Stage_Status__c = 'Active'));
        prList.add(new Product_Registration__c(Name = 'pr3', Account__c = accList[2].id, RecordTypeId = TestUtilityDataClassQantas.qbdRecordTypeId, Active__c = true)); 
        prList.add(new Product_Registration__c(Name = 'pr4', Account__c = accList[3].id, RecordTypeId = TestUtilityDataClassQantas.qbdRecordTypeId, Active__c = false));
        insert prList;
        
        Test.startTest();
            QBDOfficeId_PR_Daily_Update_Batch batchObj = new QBDOfficeId_PR_Daily_Update_Batch(false);
            Database.executeBatch(batchObj);
        Test.stopTest();        
        system.assertEquals([SELECT Key_Contact_Office_ID__c FROM Product_Registration__c WHERE id =:prList[0].id].Key_Contact_Office_ID__c, '12345');
    }
}