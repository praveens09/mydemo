/*------------------------------------------------------------
Author:        M. Sharp
Company:       Capgemini
Description:   This SOAP web service will be exposed to the iFly system to allow for real-time integration
               and Salesforce record create/update.
Inputs:        Mapping per 'Aquire File Structure v11.xlsx'
Test Class:    TBD
History
14-Oct-2016   M. Sharp     Initial Design
------------------------------------------------------------*/

global class CreateQBRRegistration {

    //Input parameters have been broken up by Salesforce object to allow for Apex limitation of 32 parameters per call.
    global class AccountInput {

        webservice String abn;
        webservice String membershipNumber;
        webservice String entityName;
        webservice String entityType;
        webservice String numberOfEmployees;
        webservice String rewardTier;
        webservice String gstRegistered;

    }

    global class ProductRegistrationInput {
        webservice String membershipNumber;
        webservice String accountStatus;
        webservice Date membershipExpiryDate;
        webservice String entityName;
        webservice String entityType;
        webservice String programBusinessName;
        webservice String accountAddressType;
        webservice String accountAddressLine1;
        webservice String accountAddressLine2;
        webservice String accountSuburbTownCity;
        webservice String accountStateCounty;
        webservice String accountPostalCode;
        webservice String accountCountry;
        webservice String industry;
        webservice String subIndustry;
        webservice String companyTurnover;
        webservice String totalActiveCards;
        webservice String frequencyOfDomesticTravel;
        webservice String frequencyOfInternationalTravel;
        webservice String businessDomesticAnnualSpend;
        webservice String businessInternationalAccountSpend;
        webservice String numberOfFrequentlyTravellingEmployees;
        webservice String numberofOccasionallyTravellingEmployees;
        webservice String numberOfRarelyTravellingEmployees;
        webservice String hasQantasClubCorporateMembership;
        webservice String qantasClubCorporateMembership;
        webservice Date enrolmentDate;
        webservice String pointBalance;
    }

    global class ContactInput {
        webservice String membershipNumber;
        webservice String accountNomineeCustomerId;
        webservice Boolean accountHolderFlag;
        webservice Boolean accountNomineeUserFlag;
        webservice Boolean keyTravelDecisionMakerFlag;
        webservice Boolean keyTravelCoordinatorFlag;
        webservice String accountNomineeStatus;
        webservice String accountNomineeStatusReason;
        webservice String accountNomineeSalutation;
        webservice String accountNomineeFirstName;
        webservice String accountNomineeLastName;
        webservice String accountNomineeEmailAddress;
        webservice String accountNomineeContactNumber;
        webservice String accountNomineeQFFNumber;
        webservice String accountNomineeCompanyRole;
        webservice Boolean accountNomineeMarketingPreference1;
        webservice Boolean accountNomineeMarketingPreference2;
        webservice Boolean accountNomineeMarketingPreference3;
        webservice Boolean accountNomineeMarketingPreference4;
        webservice Boolean accountNomineeAirTravellerStatus;
    }

    global class responseToIFLY {
        webservice String sourceCode;
        webservice String batchDate;
        webservice String recordType;
        webservice Decimal batchSequenceNumber;
        webservice String membershipNumber;
        webservice String corporateDealWithQantas;
        webservice Date dealingCustomerStartDate;
        webservice Date dealingCustomerEndDate;
        webservice String smeOverride;
        webservice String amexCoBrandedCardPartner;
        webservice String travelAgentOfQantas;
        webservice String amexEligibility;
        webservice String mcaNumber;
        webservice String aquireEligibility;
        webservice Boolean smeDeal;
        webservice Boolean qbdMember;


    }


    webservice static responseToIFLY createQBRRegistration(
        AccountInput ai,
        ProductRegistrationInput pri,
        ContactInput ci

    ) {
        String req = 'Account Input = '+String.valueOf(ai) +'; ';
        req += 'ProductRegistration Input = '+String.valueOf(pri)+'; ';
        req += 'Contact Input = '+String.valueOf(ci)+';';
        String res;
        Boolean isException = false;
        savePoint dbSavePoint = Database.setsavePoint();
        try{
            //Get Account RecordTypeId
            Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prospect Account').getRecordTypeId();


            //Build and upsert account records
            Account objAcc = new Account();

            objAcc.Aquire_Membership_Number__c = ai.membershipNumber;
            objAcc.ABN_Tax_Reference__c = ai.abn;
            objAcc.Name = ai.entityName;
            objAcc.Aquire_Entity_Type__c = ai.entityType;
            //objAcc.Acquire_Number_of_employees__c = numberOfEmployees;
            //objAcc.QBR_Reward_Tier__c = rewardTier;
            objAcc.GST_Registered__c = ai.gstRegistered;
            // NEED TO ADD THIS FIELD TO SALESFORCE - objAcc.Airline_Level__c = airlineLevel;
            objAcc.RecordTypeId = accRecordTypeId;
            //objAcc.Aquire_Override__c = 'N';
            upsert objAcc Aquire_Membership_Number__c;

            //Build and upsert Product Registration records
            Product_Registration__c objPrdReg = new Product_Registration__c();

            objPrdReg.Membership_Number__c = pri.membershipNumber;
            objPrdReg.Stage_Status__c = pri.accountStatus;
            objPrdReg.Membership_Expiry_Date__c = pri.membershipExpiryDate;
            objPrdReg.Name = pri.entityName;
            objPrdReg.Aquire_Entity_Type__c = pri.entityType;
            objPrdReg.Program_Business_Name__c = pri.programBusinessName;
            objPrdReg.Account_Address_Type_Aquire__c = pri.accountAddressType;
            objPrdReg.Account_Address_Line_1_Aquire__c = pri.accountAddressLine1;
            objPrdReg.Account_Address_Line_2_Aquire__c = pri.accountAddressLine2;
            objPrdReg.Account_Suburb_Town_City_Aquire__c = pri.accountSuburbTownCity;
            objPrdReg.Acquire_State_Province__c = pri.accountStateCounty;
            objPrdReg.Acquire_Zip_Postal_Code__c = pri.accountPostalCode;
            objPrdReg.Acquire_Country__c = pri.accountCountry;
            objPrdReg.Aquire_Industry__c = pri.industry;
            objPrdReg.Aquire_Sub_industry__c = pri.subIndustry;
            objPrdReg.Turnover__c = pri.companyTurnover;
            objPrdReg.Total_Active_Cards__c = pri.totalActiveCards;
            objPrdReg.Annual_Domestic_Flights__c = pri.frequencyOfDomesticTravel;
            objPrdReg.Annual_International_Flights__c = pri.frequencyOfInternationalTravel;
            objPrdReg.Aq_Business_Domestic_annual_spend__c = pri.businessDomesticAnnualSpend;
            objPrdReg.Aq_Business_international_annual_spend__c = pri.businessInternationalAccountSpend;
            objPrdReg.Employees_Travelling_Frequently__c = pri.numberOfFrequentlyTravellingEmployees;
            objPrdReg.Employees_Travelling_Occassionally__c = pri.numberofOccasionallyTravellingEmployees;
            objPrdReg.Employees_Travelling_Rarely__c = pri.numberOfRarelyTravellingEmployees;
            objPrdReg.Qantas_Club_Corporate_Scheme__c = pri.hasQantasClubCorporateMembership;
            objPrdReg.Qantas_Club_Corporate_Scheme_Number__c = pri.qantasClubCorporateMembership;
            objPrdReg.Enrolment_Date__c = pri.enrolmentDate;
            objPrdReg.Aquire_Point_Balance__c = pri.pointBalance;
            objPrdReg.Account__c = objAcc.id;
            upsert objPrdReg Membership_Number__c;

            //Build and upsert Associated Person records
            Associated_Person__c objAsoPerson = new Associated_Person__c();

            objAsoPerson.Aquire_Membership_Number__c = ci.membershipNumber;
            objAsoPerson.Aquire_Account_Nominee_Customer_Id__c = ci.accountNomineeCustomerId;
            objAsoPerson.Aquire_Account_Holder_Flag__c = ci.accountHolderFlag;
            objAsoPerson.Aquire_Account_Nominee_User_Flag__c = ci.accountNomineeUserFlag;
            objAsoPerson.Decision_Maker_Flag__c = ci.keyTravelDecisionMakerFlag;
            objAsoPerson.Aquire_Key_Travel_Coordinator__c = ci.keyTravelCoordinatorFlag;
            objAsoPerson.Aquire_Status__c = ci.accountNomineeStatus;
            objAsoPerson.Aquire_Account_Nominee_Status_Reason__c = ci.accountNomineeStatusReason;
            objAsoPerson.Salutation__c = ci.accountNomineeSalutation;
            objAsoPerson.First_Name__c = ci.accountNomineeFirstName;
            objAsoPerson.Last_Name__c = ci.accountNomineeLastName;
            objAsoPerson.Aquire_Email__c = ci.accountNomineeEmailAddress;
            objAsoPerson.Aquire_Phone__c = ci.accountNomineeContactNumber;
            objAsoPerson.Frequent_Flyer_Number__c = ci.accountNomineeQFFNumber;
            objAsoPerson.Job_Title__c = ci.accountNomineeCompanyRole;
            objAsoPerson.Aquire_Promotions__c = ci.accountNomineeMarketingPreference1;
            objAsoPerson.Aquire_Update_Program_News__c = ci.accountNomineeMarketingPreference2;
            objAsoPerson.Aquire_Airline_News_and_Promotions__c = ci.accountNomineeMarketingPreference3;
            objAsoPerson.Aq_Points_Balance_Account_Information__c = ci.accountNomineeMarketingPreference4;
            objAsoPerson.Aq_Account_Nominee_Air_Traveller_Status__c = ci.accountNomineeAirTravellerStatus;
            objAsoPerson.Account__c = objAcc.id;
            upsert objAsoPerson Frequent_Flyer_Number__c;

            //Build and upsert Contact records
            Contact objCon = new Contact();
            objCon.Aquire_Membership_Number__c = ci.membershipNumber;
            objCon.Aquire_Account_Nominee_customer_Id__c = ci.accountNomineeCustomerId;
            objCon.Aquire_Account_Holder_Flag__c = ci.accountHolderFlag;
            objCon.Aquire_Account_Nominee_User_Flag__c = ci.accountNomineeUserFlag;
            objCon.Aquire_Key_Decision_Maker__c = ci.keyTravelDecisionMakerFlag;
            objCon.Aquire_Key_Travel_Coordinator__c = ci.keyTravelCoordinatorFlag;
            objCon.Aquire_Status__c = ci.accountNomineeStatus;
            objCon.Account_Nominee_Status_reason__c = ci.accountNomineeStatusReason;
            objCon.Salutation = ci.accountNomineeSalutation;
            objCon.FirstName = ci.accountNomineeFirstName;
            objCon.LastName = ci.accountNomineeLastName;
            objCon.Email = ci.accountNomineeEmailAddress;
            objCon.Phone = ci.accountNomineeContactNumber;
            objCon.Frequent_Flyer_Number__c = ci.accountNomineeQFFNumber;
            objCon.Job_Title__c = ci.accountNomineeCompanyRole;
            objCon.Aquire_Promotions__c = ci.accountNomineeMarketingPreference1;
            objCon.Aquire_Updates_Program_News__c = ci.accountNomineeMarketingPreference2;
            objCon.Aquire_Airline_News_and_Promotions__c = ci.accountNomineeMarketingPreference3;
            objCon.Aq_Points_Balance_Account_Information__c = ci.accountNomineeMarketingPreference4;
            objCon.Aq_Account_Nominee_Air_Traveller_Status__c = ci.accountNomineeAirTravellerStatus;
            upsert objCon Frequent_Flyer_Number__c;

            //Building response
            List<Product_Registration__c> lstPrdReg = [SELECT Membership_Number__c, Account__r.Dealing_flag__c, Account__r.Contract_Commencement_Date__c, 
                                                       Account__r.Contract_Expiry_Date__c, Account__r.Aquire_Override__c, Account__r.Agency__c, 
                                                       Account__r.AMEX_response_Code__c, MCA_Number__c, Account__r.Aquire_Eligibility_f__c, Account__r.SME_Deal__c, 
                                                       Account__r.QBD__c, Account__r.Aquire_Membership_Number__c FROM Product_registration__c 
                                                       WHERE Membership_Number__c = : pri.membershipNumber LIMIT 1];

            responseToIFLY response = new responseToIFLY();
            response.sourceCode = 'QUEST';
            response.batchDate = String.valueOf(Date.today());
            response.recordType = '2';
            response.batchSequenceNumber = 000000001;
            response.membershipNumber = lstPrdReg[0].Account__r.Aquire_Membership_Number__c;
            response.corporateDealWithQantas = lstPrdReg[0].Account__r.Dealing_flag__c;
            response.dealingCustomerStartDate = lstPrdReg[0].Account__r.Contract_Commencement_Date__c;
            response.dealingCustomerEndDate = lstPrdReg[0].Account__r.Contract_Expiry_Date__c;
            response.smeOverride = lstPrdReg[0].Account__r.Aquire_Override__c;
            response.amexCoBrandedCardPartner = 'N';
            response.travelAgentOfQantas = lstPrdReg[0].Account__r.Agency__c;
            response.amexEligibility = lstPrdReg[0].Account__r.AMEX_response_Code__c;
            response.mcaNumber = lstPrdReg[0].MCA_Number__c;
            response.aquireEligibility = lstPrdReg[0].Account__r.Aquire_Eligibility_f__c;
            response.smeDeal = lstPrdReg[0].Account__r.SME_Deal__c;
            response.qbdMember = lstPrdReg[0].Account__r.QBD__c;       
            res = 'Response = '+String.valueOf(response)+';';
            return response;
        }catch(Exception ex){
            //To do logic to save exception in salesforce
            system.debug('Exp MSG#####'+ex);
            Database.rollBack(dbSavePoint);
            isException = true;
            List<String> lstErrMsg = new List<String>();
            lstErrMsg.add('CreateQBRRegistration service');
            lstErrMsg.add('CreateQBRRegistration');
            lstErrMsg.add('createQBRRegistration');
            lstErrMsg.add(UserInfo.getUserName());
            Map<String, String> mapMsg= new Map<String, String>();
            mapMsg.put('Inbound', req);
            mapMsg.put('Outbound', String.valueOf(res));
            CreateLogs.createApplicationLog('', ex, lstErrMsg, mapMsg);
        }
        finally{
            List<Integration_Logs__c> lstIntegrationLog = new List<Integration_Logs__c>();
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('CreateQBRRegistration Service',String.valueOf(req),'Inbound Service','' ,isException));
            lstIntegrationLog.add(CreateLogs.createIntegrationLog('CreateQBRRegistration Service',String.valueOf(res),'Acknowledgement','' ,isException));
            insert lstIntegrationLog;
        }
        return null;
    }
}