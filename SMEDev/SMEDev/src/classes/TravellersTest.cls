/*==================================================================================================================================
Author:         Praveen Sampath
Company:        Capgemini
Purpose:        Test Class for CreateQBRRegistration Class
Date:           25/10/2016          
History:

==================================================================================================================================

==================================================================================================================================*/

@isTest(SeeAllData = false)
private class TravellersTest{

	@testSetup
	static void createTestData(){
		List<Trigger_Status__c>tsList = new List<Trigger_Status__c>();

		Trigger_Status__c accTrg = new Trigger_Status__c();
        accTrg.Name = 'AccountTeam';
        accTrg.Active__c = false;
        tsList.add(accTrg);

        Trigger_Status__c accEligibility = new Trigger_Status__c();
        accEligibility.Name = 'AccountEligibilityLogTrigger';
        accEligibility.Active__c = false;
        tsList.add(accEligibility);

        Trigger_Status__c accAirLevel = new Trigger_Status__c();
        accAirLevel.Name = 'AccountAirlineLeveltrigger';
        accAirLevel.Active__c = false;
        tsList.add(accAirLevel);
        

        Trigger_Status__c memTrg = new Trigger_Status__c();
        memTrg.Name = 'MemberTrigger';
        memTrg.Active__c = false;
        tsList.add(memTrg);

        insert tsList;

		List<QantasConfigData__c> lstConfigData = new List<QantasConfigData__c>();
		lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('DeleteTraveller_PostNoDataFoundCode', '400'));
		lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('DeleteTraveller_PostExceptionCode', '500'));
		lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('DeleteTraveller_PostSucessCode', '200'));
		lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Traveller_GetNoDataFoundCode', '400'));
		lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Traveller_GetExceptionCode', '500'));
		lstConfigData.add(TestUtilityDataClassQantas.createQantasConfigData('Traveller_GetSucessCode', '200'));
		insert lstConfigData;

		Account objAcc = TestUtilityDataClassQantas.createAccount();
		objAcc.ABN_Tax_Reference__c = '999999';
		insert objAcc;
		system.assert(objAcc.Id != Null, 'Account is not inserted');


		List<Account> lstAcc = new List<Account>();
		lstAcc.add(objAcc);
		List<Associated_Person__c> lstAsoPerson = TestUtilityDataClassQantas.getMembers(lstAcc);
		system.assert(lstAsoPerson != Null, 'Associated_Person__c is not inserted');
		lstAsoPerson[0].Active_QBD_Record__c = true;
		update lstAsoPerson[0];
	}

	static TestMethod void DoGetPostiveTest(){
		RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();

	    // pass the req and resp objects to the method     
	    req.requestURI = '/services/apexrest/Travellers/999999';  
	    req.httpMethod = 'GET';
	    //req.requestBody = blob.valueOf(reqBody);
	    RestContext.request = req;
	    RestContext.response = res;
	    Travellers.doGet();
	    system.assert(res.statusCode == 200, 'Get Postive Status Code is not Matching');
	}

	static TestMethod void DoGetNoDataTest(){
		RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();

	    // pass the req and resp objects to the method     
	    req.requestURI = '/services/apexrest/Travellers/9999999';  
	    req.httpMethod = 'GET';
	    //req.requestBody = blob.valueOf(reqBody);
	    RestContext.request = req;
	    RestContext.response = res;
	    Travellers.doGet();
	    system.assert(res.statusCode == 400, 'Get No Data  Status Code is not Matching');
	}

	static TestMethod void DoGetExceptionTest(){
		QantasConfigData__c objQantas = [select id from QantasConfigData__c where Name = 'Traveller_GetSucessCode'];
		delete objQantas;

		RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();

	    // pass the req and resp objects to the method     
	    req.requestURI = '/services/apexrest/Travellers/999999';  
	    req.httpMethod = 'GET';
	    //req.requestBody = blob.valueOf(reqBody);
	    RestContext.request = req;
	    RestContext.response = res;
	    Travellers.doGet();
	    system.assert(res.statusCode == 500, 'Get Exception Status Code is not Matching');
	}

	static TestMethod void DoPatchPostiveTest(){
		String reqBody = '{	"travellers": [ {"ffNumber": "7799339937","lastName": "Plum","firstName": "Victoria","title": "Mrs"}';
		reqBody += ' ]}';

		RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();

	    // pass the req and resp objects to the method     
	    req.requestURI = '/services/apexrest/Travellers/999999';  
	    req.httpMethod = 'PATCH';
	    req.requestBody = blob.valueOf(reqBody);
	    RestContext.request = req;
	    RestContext.response = res;
	    Travellers.doPatch();
	    system.assert(res.statusCode == 200, 'Patch Exception Status Code is not Matching');
	}

	static TestMethod void DoPatchNoDataTest(){
		String reqBody = '{	"travellers": [ {"ffNumber": "7799339937","lastName": "Plum","firstName": "Victoria","title": "Mrs"}';
		reqBody += ' ]}';

		RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();

	    // pass the req and resp objects to the method     
	    req.requestURI = '/services/apexrest/Travellers/9999999';  
	    req.httpMethod = 'PATCH';
	    req.requestBody = blob.valueOf(reqBody);
	    RestContext.request = req;
	    RestContext.response = res;
	    Travellers.doPatch();
	    system.assert(res.statusCode == 400, 'Patch Exception Status Code is not Matching');
	}

	static TestMethod void DoPatchExceptionTest(){
		QantasConfigData__c objQantas = [select id from QantasConfigData__c where Name = 'DeleteTraveller_PostSucessCode'];
		delete objQantas;

		String reqBody = '{	"travellers": [ {"ffNumber": "7799339937","lastName": "Plum","firstName": "Victoria","title": "Mrs"}';
		reqBody += ' ]}';

		RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();

	    // pass the req and resp objects to the method     
	    req.requestURI = '/services/apexrest/Travellers/999999';  
	    req.httpMethod = 'PATCH';
	    req.requestBody = blob.valueOf(reqBody);
	    RestContext.request = req;
	    RestContext.response = res;
	    Travellers.doPatch();
	    system.assert(res.statusCode == 500, 'Patch Exception Status Code is not Matching');
	}
}