/*==================================================================================================================================
Author:         Praveen Sampath
Company:        Capgemini
Purpose:        Test Class for CreateQBRRegistration Class
Date:           25/10/2016          
History:

==================================================================================================================================

==================================================================================================================================*/

@isTest(SeeAllData = false)
private class CreateQBRRegistrationTest
{
	@testSetup
	static void createTestData(){
		
	}

	/*************************************************************************************
    Method Name: testCallBack
    Description: To test createQBRRegistration method
    ***************************************************************************************/ 
    static TestMethod void testCallBack()
	{
        CreateQBRRegistration.AccountInput accInputWrapper = testUtilityDataClassQantas.craeteAccountInput();
		CreateQBRRegistration.ProductRegistrationInput prdRegInputWrapper = testUtilityDataClassQantas.createProductRegInput();
		CreateQBRRegistration.ContactInput conInputWrapper = testUtilityDataClassQantas.createContactInput();
		system.assert(accInputWrapper != Null, 'AccountInput is Null not intialized');
		system.assert(prdRegInputWrapper != Null, 'ProductRegistrationInput is Null not intialized');
		system.assert(conInputWrapper != Null, 'ContactInput is Null not intialized');
		
		CreateQBRRegistration.responseToIFLY responseWrapper =  CreateQBRRegistration.createQBRRegistration(accInputWrapper, prdRegInputWrapper, conInputWrapper);
		CreateQBRRegistration.responseToIFLY expectedResponse = testUtilityDataClassQantas.createExpectedResponse( prdRegInputWrapper.membershipNumber );
		system.assert(responseWrapper != Null, 'Response is Null');
		system.assert(responseWrapper.batchDate == expectedResponse.batchDate, 'Response is not generaated as Expected');
	}
}