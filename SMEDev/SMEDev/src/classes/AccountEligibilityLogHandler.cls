/*****************************************************************************************************
ClassName 	: AccountEligibilityLogHandler
CreatedBy 	: Gourav Bhardwaj
Jira 		: 1787
Description	: Add functionality to capture Aquire Override Flag Start/End Dates and their change history.
TestClass	: TestAccountEligibilityLogHandler

Change History:
======================================================================================================================
Name                	Jira    	Description                                             	Tag
======================================================================================================================
Gourav Bhardwaj     	1821    	Add functionality to capture Corporate Deal Flag 			T01
									Start/End Dates change history in Eligibility Log
									

Gourav Bhardwaj     	1898    	Create AccountEligibilityLog record when Account			T02
									record is created							
									
******************************************************************************************************/


public class AccountEligibilityLogHandler {
    public static List<Account_Eligibility_Log__c> insertUpdateList = new List<Account_Eligibility_Log__c>();
    
    //Set of records which are inserted, which be used for intraday records update. 
    public static Set<Id> insertUpdateAcountSet = new Set<Id>();
    public static List<Account> accountUpdateList = new List<Account>();
    
    //Map of AccountId:FieldName to field which needs to be updated.
    private static Map<String,String> accountToField = new Map<String,String>();
    private static String aquireOverrideLabel = Schema.Account.fields.Aquire_Override__c.getDescribe().getLabel();  
    private static String travelAgentLabel = Schema.Account.fields.Agency__c.getDescribe().getLabel();
    
    public static void logAccountEligibility(Map<Id,Account> newMapAccount,Map<Id,Account> oldMapAccount,Boolean checkRecursive){
        
        if(Test.isRunningTest()){
            checkRecursive = true;
        }
        
        for(Id accountId : newMapAccount.keySet()){
            Account acc1 = newMapAccount.get(accountId);
            if(checkRecursive){
                aquireEligibilityLogAquireOverride(acc1,oldMapAccount);
            }
            aquireEligibilityLogTravelAgent(acc1,oldMapAccount);
        }
        
        Map<String,Account_Eligibility_Log__c> mapAccountToAccountEligibility = new Map<String,Account_Eligibility_Log__c>();
        
        system.debug('### Update Account List'+accountUpdateList);
        
        List<Account_Eligibility_Log__c> listDeleteAccountElibilityLog = new List<Account_Eligibility_Log__c>();
        
        if(accountUpdateList.size()>0){
            
            for(Account_Eligibility_Log__c aeh : [SELECT CreatedDate,LastModifiedDate,Value__c,EndDate__c,StartDate__c,FieldName__c,Account__c,Id FROM Account_Eligibility_Log__c WHERE Account__c IN:accountUpdateList order by CreatedDate desc]){
                Account_Eligibility_Log__c aeh1 = new Account_Eligibility_Log__c(Id = aeh.Id);
                
                system.debug('### outside  : '+checkRecursive+' : '+insertUpdateAcountSet.contains(aeh.Account__c)+':'+aeh.FieldName__c+' : '+aeh.StartDate__c);
                
                //Logic for Travel Agent ---------------------------------------------
                
                //Intraday Logic : If changes to the Travel Agent flag is made in the same day, keep only the latest change.
                if(insertUpdateAcountSet.contains(aeh.Account__c) && accountToField.get(aeh.Account__c+':'+aeh.FieldName__c)==travelAgentLabel && aeh.StartDate__c==Date.today()){
                    listDeleteAccountElibilityLog.add(aeh);
                }
                
                //Previous Record : Update the End date for the previous travel agent record in AEL Object.
                if(aeh.FieldName__c==travelAgentLabel && accountToField.get(aeh.Account__c+':'+aeh.FieldName__c)==travelAgentLabel && aeh.StartDate__c!=Date.today()){
                    aeh1.EndDate__c = Date.today().addDays(-1);
                    
                    if(mapAccountToAccountEligibility.containsKey(aeh.Account__c+':'+aeh.FieldName__c)){
                        if(mapAccountToAccountEligibility.get(aeh.Account__c+':'+aeh.FieldName__c).CreatedDate < aeh.CreatedDate ){
                            mapAccountToAccountEligibility.put(aeh.Account__c+':'+aeh.FieldName__c,aeh1);
                        }
                    }else{
                        mapAccountToAccountEligibility.put(aeh.Account__c+':'+aeh.FieldName__c,aeh1);
                    }
                    
                }
                //Travel Agent End ---------------------------------------------
                
                
                //Aquire Override ---------------------------------------------
                
                //Intraday Logic : If changes to the Aquire Override flag is made in the same day, keep only the latest change.
                if(checkRecursive && insertUpdateAcountSet.contains(aeh.Account__c) && accountToField.get(aeh.Account__c+':'+aeh.FieldName__c)==aquireOverrideLabel && aeh.StartDate__c==Date.today()){
                    listDeleteAccountElibilityLog.add(aeh);
                }
                
                
                if(checkRecursive){
                    
                    //Previous Record : Update the End date for the previous Aquire Override record in AEL Object.
                    if(accountToField.get(aeh.Account__c+':'+aeh.FieldName__c)==aquireOverrideLabel && aeh.StartDate__c!=Date.today()){
                        
                        aeh1.EndDate__c = Date.today().addDays(-1);  
                        
                        if(mapAccountToAccountEligibility.containsKey(aeh.Account__c+':'+aeh.FieldName__c)){
                            if(mapAccountToAccountEligibility.get(aeh.Account__c+':'+aeh.FieldName__c).CreatedDate  < aeh.CreatedDate ){
                                mapAccountToAccountEligibility.put(aeh.Account__c+':'+aeh.FieldName__c,aeh1);
                            }
                        }else{
                            mapAccountToAccountEligibility.put(aeh.Account__c+':'+aeh.FieldName__c,aeh1);
                        }
                        
                    }else 
                        //Update the End date for Aquire Override record in AEL Object when the Flag is Y.
                        if(aeh.Value__c=='Y' && accountToField.get(aeh.Account__c+':'+aeh.FieldName__c)=='Y'){
                            aeh1.EndDate__c =  newMapAccount.get(aeh.Account__c).Aquire_Override_End_Date__c;
                            if(mapAccountToAccountEligibility.containsKey(aeh.Account__c+':'+aeh.FieldName__c)){
                                if(mapAccountToAccountEligibility.get(aeh.Account__c+':'+aeh.FieldName__c).CreatedDate  < aeh.CreatedDate ){
                                    mapAccountToAccountEligibility.put(aeh.Account__c+':'+aeh.FieldName__c,aeh1);
                                }
                            }else{
                                mapAccountToAccountEligibility.put(aeh.Account__c+':'+aeh.FieldName__c,aeh1);
                            }
                        }
                } 
                //Aquire Override End ---------------------------------------------
                
            }
            
            system.debug('### Map : '+mapAccountToAccountEligibility);
            for(String accoungIdField : mapAccountToAccountEligibility.keySet()){
                insertUpdateList.add(mapAccountToAccountEligibility.get(accoungIdField));
            }
        }        
        
        
        if(listDeleteAccountElibilityLog.size()>0){
            delete listDeleteAccountElibilityLog;
        }
        
        
        if(insertUpdateList.size()>0){
            system.debug('### InsertUpdate List : '+insertUpdateList);
            upsert insertUpdateList;
        }
        
    }
    
    //Account Eligibility Log creation for Aquire Override.
    private static void aquireEligibilityLogAquireOverride(Account acc1,Map<Id,Account> oldMapAccount){
        if(oldMapAccount.get(acc1.Id).Aquire_Override__c == 'N' && acc1.Aquire_Override__c=='Y' ){
            
            Account_Eligibility_Log__c aeh = new Account_Eligibility_Log__c();
            aeh.FieldName__c = aquireOverrideLabel;
            aeh.Value__c = acc1.Aquire_Override__c;
            aeh.StartDate__c = Date.today();
            aeh.EndDate__c = acc1.Aquire_Override_End_Date__c;
            aeh.Account__c = acc1.Id;
            
            insertUpdateList.add(aeh);
            insertUpdateAcountSet.add(acc1.Id); //For removing Intraday
            accountUpdateList.add(acc1); 
            accountToField.put(acc1.Id+':'+aquireOverrideLabel,aquireOverrideLabel); //For Previous Record Update
        }else if(oldMapAccount.get(acc1.Id).Aquire_Override__c == 'Y' && acc1.Aquire_Override__c=='N'){
            Account_Eligibility_Log__c aeh1 = new Account_Eligibility_Log__c();
            aeh1.FieldName__c = aquireOverrideLabel;
            aeh1.Value__c = acc1.Aquire_Override__c;
            aeh1.StartDate__c = Date.today();
            aeh1.EndDate__c = date.newinstance(3999,12,31);
            aeh1.Account__c = acc1.Id;
            
            insertUpdateList.add(aeh1);
            insertUpdateAcountSet.add(acc1.Id); //For removing Intraday
            accountUpdateList.add(acc1); 
            accountToField.put(acc1.Id+':'+aquireOverrideLabel,aquireOverrideLabel); //For Previous Record Update
            
        }else if(oldMapAccount.get(acc1.Id).Aquire_Override__c == 'Y' && acc1.Aquire_Override__c=='Y' 
                 && oldMapAccount.get(acc1.Id).Aquire_Override_End_Date__c != acc1.Aquire_Override_End_Date__c ){
                     accountUpdateList.add(acc1); 
                     accountToField.put(acc1.Id+':'+aquireOverrideLabel,'Y');
                 }
    }
    
    //Account Eligibility Log creation for Travel Agent.
    private static void aquireEligibilityLogTravelAgent(Account acc1,Map<Id,Account> oldMapAccount){
        
        if(oldMapAccount.get(acc1.Id).Agency__c == 'N' && acc1.Agency__c=='Y' ){
            
            Account_Eligibility_Log__c aeh = new Account_Eligibility_Log__c();
            aeh.FieldName__c = travelAgentLabel;
            aeh.Value__c = acc1.Agency__c;
            aeh.StartDate__c = Date.today();
            aeh.EndDate__c = date.newinstance(3999,12,31);
            aeh.Account__c = acc1.Id;
            
            insertUpdateList.add(aeh);
            insertUpdateAcountSet.add(acc1.Id); //For removing Intraday
            accountUpdateList.add(acc1);
            accountToField.put(acc1.Id+':'+travelAgentLabel,travelAgentLabel);  
            
        }else if(oldMapAccount.get(acc1.Id).Agency__c == 'Y' && acc1.Agency__c=='N'){
            Account_Eligibility_Log__c aeh1 = new Account_Eligibility_Log__c();
            aeh1.FieldName__c = travelAgentLabel;
            aeh1.Value__c = acc1.Agency__c;
            aeh1.StartDate__c = Date.today();
            aeh1.EndDate__c = date.newinstance(3999,12,31);
            aeh1.Account__c = acc1.Id;
            
            insertUpdateList.add(aeh1);
            insertUpdateAcountSet.add(acc1.Id); //For removing Intraday
            accountUpdateList.add(acc1);
            accountToField.put(acc1.Id+':'+travelAgentLabel,travelAgentLabel); 
        }
    }
    
    
    /****************** T01 ********************/
    public static void logAccountEligibilityContracts(Boolean checkRecursive,Map<Id,Contract__c> contractNewMap,Map<Id,Contract__c> contractOldMap){
        
        //Populating Custom Settings values, this will be used to filter only those Contracts and Agreements 
        //records which have the defined Categories
        List<Contract_Categories__c> contractCategoriesList = Contract_Categories__c.getall().values();
        Set<String> contractCategoriesSet = new Set<String>();
        
        for(Contract_Categories__c categories : contractCategoriesList){
            contractCategoriesSet.add(categories.Name);    
        }
        
        system.debug('### Custom Settings Set : '+contractCategoriesSet);	
        
        List<Account_Eligibility_Log__c> upsertAccountEligibilityLog = new List<Account_Eligibility_Log__c>();
        List<Contract__c> updateListContract = new List<Contract__c>();
        
        if(Test.isRunningTest()){
            checkRecursive = True;
        }
        
		Set<Id> setContractId = new Set<Id>();
		Map<Id,Id> mapContractToAccountEligibility = new Map<Id,Id>();
		
		for(Account_Eligibility_Log__c accountEligibility : [SELECT Id,ContractAndAgreement__c FROM Account_Eligibility_Log__c WHERE ContractAndAgreement__c IN: contractNewMap.keySet()]){
			system.debug('### eligibility log : '+accountEligibility);
			setContractId.add(accountEligibility.ContractAndAgreement__c);
			mapContractToAccountEligibility.put(accountEligibility.ContractAndAgreement__c,accountEligibility.Id);
		}
		
		
        for(Id contractId : contractNewMap.keySet()){
            
            //Contracts and Agreements record which match the criteria are inserted into the Account Eligibility log Object.
            if(contractOldMap.get(contractId).Status__c != contractNewMap.get(contractId).Status__c 
               && contractCategoriesSet.contains(contractNewMap.get(contractId).Type__c)
               && contractNewMap.get(contractId).Active__c && checkRecursive){
				   
				   
				   if((mapContractToAccountEligibility.keySet()).contains(contractId)){
					   //Update the End Date in the Account Eligibility Log Object
					system.debug('### Record Present : '+mapContractToAccountEligibility.get(contractId));   
			Account_Eligibility_Log__c accountEligibilityLog = new Account_Eligibility_Log__c(Id = mapContractToAccountEligibility.get(contractId));
			accountEligibilityLog.StartDate__c = contractNewMap.get(contractId).Contract_Start_Date__c;
            accountEligibilityLog.EndDate__c = contractNewMap.get(contractId).Contract_End_Date__c;
					   
			upsertAccountEligibilityLog.add(accountEligibilityLog);
					   
				   }else 
					if(contractNewMap.get(contractId).Status__c=='Signed by Customer'){
				   system.debug('### Inserting');
                        
				   Account_Eligibility_Log__c accountEligibilityLog = new Account_Eligibility_Log__c();
                   accountEligibilityLog.FieldName__c = 'Corporate Deal';
                   accountEligibilityLog.Value__c = 'Y';
                   accountEligibilityLog.StartDate__c = contractNewMap.get(contractId).Contract_Start_Date__c;
                   accountEligibilityLog.EndDate__c = contractNewMap.get(contractId).Contract_End_Date__c;
                   accountEligibilityLog.Account__c = contractNewMap.get(contractId).Account__c;
                   accountEligibilityLog.ContractAndAgreement__c = contractNewMap.get(contractId).Id;
                   
                   upsertAccountEligibilityLog.add(accountEligibilityLog);
					   
				   }
				   
		  }
            
            //Updating the Contracts and Agreements records present in the Account Eligibility Log Object on Start Date or End Date change.
            if(contractNewMap.get(contractId).Status__c=='Signed by Customer' && contractNewMap.get(contractId).Active__c
               && (contractNewMap.get(contractId).Contract_Start_Date__c != contractOldMap.get(contractId).Contract_Start_Date__c
                   || contractNewMap.get(contractId).Contract_End_Date__c != contractOldMap.get(contractId).Contract_End_Date__c)
              ){
                  updateListContract.add(contractNewMap.get(contractId));
               }
            
            if(updateListContract.size()>0){
                for(Account_Eligibility_Log__c accountEligibility : [SELECT Id,ContractandAgreement__c,EndDate__c,StartDate__c from Account_Eligibility_Log__c WHERE ContractandAgreement__c IN: updateListContract]){
                    Account_Eligibility_Log__c accountEligibilityLog = new Account_Eligibility_Log__c(Id=accountEligibility.Id);
                    accountEligibilityLog.StartDate__c = contractNewMap.get(accountEligibility.ContractandAgreement__c).Contract_Start_Date__c;
                    accountEligibilityLog.EndDate__c = contractNewMap.get(accountEligibility.ContractandAgreement__c).Contract_End_Date__c;
                    upsertAccountEligibilityLog.add(accountEligibilityLog);
                }
            }
        }
        
        if(upsertAccountEligibilityLog.size()>0){
            system.debug('### UpsertList : '+upsertAccountEligibilityLog);
            upsert upsertAccountEligibilityLog;
        }
    }
    /****************** T01 Ends********************/
	
	
	/****************** T02 ********************/
	 public static void logAccountEligibilityOnAccountCreate(Map<Id,Account> newMapAccount){
		 List<Account_Eligibility_Log__c> insertAccountEligibilityLog = new List<Account_Eligibility_Log__c>();
		 system.debug('### Insert Eligibility Log');
		 for(Id accountId : newMapAccount.keySet()){
			 
			 system.debug('### '+travelAgentLabel+' : '+newMapAccount.get(accountId).Agency__c);
			 
			 system.debug('### '+aquireOverrideLabel+' : '+newMapAccount.get(accountId).Aquire_Override__c);
			 
			 //Travel Agent
				   Account_Eligibility_Log__c accountEligibilityLog = new Account_Eligibility_Log__c();
				   accountEligibilityLog.FieldName__c = travelAgentLabel;
                   accountEligibilityLog.StartDate__c = Date.Today();
                   accountEligibilityLog.EndDate__c = Date.newinstance(3999,12,31);
                   accountEligibilityLog.Account__c = accountId;
             
             		if(newMapAccount.get(accountId).Industry=='Travel'){
                 	accountEligibilityLog.Value__c =  'Y';
             		}else{
                 	accountEligibilityLog.Value__c =  'N';
             		}
		
				insertAccountEligibilityLog.add(accountEligibilityLog);
				
				


			 //Aquire Override
				   Account_Eligibility_Log__c accountEligibilityLog1 = new Account_Eligibility_Log__c();
				   accountEligibilityLog1.FieldName__c = aquireOverrideLabel;
             	   accountEligibilityLog1.Value__c =  newMapAccount.get(accountId).Aquire_Override__c;
                   accountEligibilityLog1.StartDate__c = Date.Today();
				   accountEligibilityLog1.Account__c = accountId;
             
				   if(newMapAccount.get(accountId).Aquire_Override__c=='Y'){
					accountEligibilityLog1.EndDate__c = newMapAccount.get(accountId).Aquire_Override_End_Date__c;
				   }else{
					accountEligibilityLog1.EndDate__c = Date.newinstance(3999,12,31);
				   }
				   
				   
				insertAccountEligibilityLog.add(accountEligibilityLog1); 
			 
		 }
		 
		 if(insertAccountEligibilityLog.size()>0){
            system.debug('### Insert : '+insertAccountEligibilityLog);
            insert insertAccountEligibilityLog;
        }
		
		 
	 }
	 /****************** T02 Ends********************/
}