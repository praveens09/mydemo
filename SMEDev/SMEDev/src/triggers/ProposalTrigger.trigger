trigger ProposalTrigger on Proposal__c (Before Update, After Update) {
    try{ 
        Trigger_Status__c ts = Trigger_Status__c.getValues('ProposalTrigger');
        if(ts.Active__c){
         
            if(Trigger.isAfter && Trigger.isUpdate){
                ProposalTriggerHandler.approvedByInternalTeam(Trigger.New, Trigger.OldMap);
                ProposalTriggerHandler.revenueChanges(Trigger.New, Trigger.OldMap); 
            }
            
            if(Trigger.isBefore && Trigger.isUpdate){        
                ProposalTriggerHandler.statusChange(Trigger.New, Trigger.OldMap);    
            }  
            
            if(Trigger.isAfter && Trigger.isUpdate){
                ProposalTriggerHandler.changeDiscountListRecordType(Trigger.New);  
            }   
        }         
    }catch(Exception e){
        System.debug('Error Occured From Proposal Trigger: '+e.getMessage());
    }
}