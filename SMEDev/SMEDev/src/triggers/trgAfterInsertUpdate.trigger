trigger trgAfterInsertUpdate on Product_Registration__c (after insert,after update) {
    clsAccountEligibilityHelper.updateAmexDetailsOnAquire(trigger.new);
    clsAquireRegistrationsHelper.updateRelatedAquireMembers(trigger.newMap);
}