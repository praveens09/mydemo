trigger trgCaseFieldUpdates on Case (before update) {
    try{
        Trigger_Status__c ts = Trigger_Status__c.getValues('trgCaseFieldUpdates');
        if(ts.Active__c){   
            clsCaseFieldUpdatesHelper.generateAuthorityNumber(trigger.new);
        }   
    }catch(Exception e){
        System.debug('Error Occured : '+e.getMessage());
    }
}