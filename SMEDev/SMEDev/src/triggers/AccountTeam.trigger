/**********************************************************************************************************************************
 
    Created Date: 25/02/2015
 
    Description: Process Account trigger for:
                 1. Assign account team members based on user department after insert.
                 2. Opportunity creation based on travel frequency before insert.
  
    Versión:
    V1.0 - 25/02/2015 - Initial version [FO]

Change History:
======================================================================================================================
Name                Jira    Description                                             Tag
======================================================================================================================
Gourav Bhardwaj     1787    Add functionality to capture Aquire Override            T01
                            Flag Start/End Dates and their change history.

Gourav Bhardwaj     1898    Create AccountEligibilityLog record when Account        T02
                            record is created                           

**********************************************************************************************************************************/

trigger AccountTeam on Account (before update,after update,after insert) {  
    try{
    Trigger_Status__c ts = Trigger_Status__c.getValues('AccountTeam');
        
    
    if(ts.Active__c){
    
        if (Trigger.isUpdate) {
            
            /* Before insert process */
            if (Trigger.isBefore) { 
                         
                AccountTeamHandler accHandler = new AccountTeamHandler();
                
                accHandler.setRevenueManualUpdate(Trigger.New, Trigger.oldMap);
                
                /* Assign & create Account team members based on user department*/
                accHandler.createAccountTeamMembers(Trigger.New, Trigger.oldMap);
                
            }        
            
         }       
    }  
        
    Trigger_Status__c triggerStatus = Trigger_Status__c.getValues('AccountEligibilityLogTrigger');        
    if(triggerStatus.Active__c){
        
    /*******  T01  ***********/
         if (Trigger.isAfter && Trigger.isUpdate) { 
            AccountEligibilityLogHandler.logAccountEligibility(Trigger.newMap,Trigger.oldMap,checkRecursive.runOnce());
          }
    /*******  T01  Ends********/
          
    /*******  T02  ***********/
         if (Trigger.isAfter && Trigger.isInsert) { 
            AccountEligibilityLogHandler.logAccountEligibilityOnAccountCreate(Trigger.newMap);
          }
    /*******  T02  Ends***********/
          
    }   
    if(!AccountAirlineLevelHandler.isRun && Trigger_Status__c.getValues('AccountAirlineLeveltrigger').Active__c && Trigger.isBefore && Trigger.isUpdate) {
        AccountAirlineLevelHandler.updateDiscountCodeOnProducts(Trigger.newMap,Trigger.oldMap);
    }
      
    }catch(Exception e){
        System.debug('Error Occured From Account Trigger: '+e.getMessage());
    }  
  
}