<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Integration</defaultLandingTab>
    <description>Informatica Cloud provides data integration and data quality services for Salesforce.</description>
    <label>Informatica Cloud</label>
    <tab>Integration</tab>
    <tab>Regional_Route_MAT__c</tab>
    <tab>Market_Intelligence__c</tab>
</CustomApplication>
